# Atyos CLI - Devops Tools

[![pipeline status](https://gitlab.com/atyos/production/public/atyos-cli/badges/main/pipeline.svg)](https://gitlab.com/atyos/production/public/atyos-cli/-/commits/main)

Atyos CLI provide some commands to help you with devops stuff.

## Installation

You can download the binary from the releases page : <https://gitlab.com/atyos/public/atyos-cli/-/releases>

```bash
curl https://gitlab.com/api/v4/projects/32423644/packages/generic/atyos-cli/[VERSION]/atyos --output atyos
chmod u+x atyos
mv atyos /usr/local/bin/atyos
```

or pull the docker image :

```bash
docker pull atyos/atyos_cli
```

## How to use

### Sam templates

```bash
# Deploy sam templates
atyos sam-templates/deploy
```

### CICD Components

```bash
# Bump git tag version
atyos version/bump
```

### Artifacts management

```bash
# Push artifacts
atyos artifacts/push
```

```bash
# Pull artifacts
atyos artifacts/pull
```

### Terraform templates

```bash
# Validate terraform configuration
atyos terraform/validate
```

```bash
# Apply terraform configuration without prompt
atyos terraform/apply
```

## DRS component Usage

[DRS Documentation](./components/drs/README.md) 