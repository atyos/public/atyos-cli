FROM ubuntu:22.04

ARG TERRAFORM_VERSION=1.7.1
ARG ATYOSCLI_PATH

## APT noninteractive
ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true

RUN chmod -R a+r /etc/apt/trusted.gpg.d/ \
    && apt update  \
    && apt upgrade -y \
    && apt install -y curl unzip tar gzip software-properties-common docker.io wget

# Install Docker BuildKit and Notary signing tool
RUN apt install docker.io \
    && mkdir -p $HOME/.docker/cli-plugins \
    && wget https://github.com/docker/buildx/releases/download/v0.12.1/buildx-v0.12.1.linux-amd64 \
    && chmod +x buildx-v0.12.1.linux-amd64 \
    && mv buildx-v0.12.1.linux-amd64 $HOME/.docker/cli-plugins/docker-buildx \
    && wget https://d2hvyiie56hcat.cloudfront.net/linux/amd64/installer/deb/latest/aws-signer-notation-cli_amd64.deb \
    && dpkg -i aws-signer-notation-cli_amd64.deb \
    && rm aws-signer-notation-cli_amd64.deb

# Install python 3.9, python3.10 and python3.11
RUN add-apt-repository -y ppa:deadsnakes/ppa \
    && echo 'tzdata tzdata/Areas select Etc' | debconf-set-selections \
    && echo 'tzdata tzdata/Zones/Etc select UTC' | debconf-set-selections \
    && apt install -y python3.9 python3.9-distutils python3.9-venv python3.10 python3.10-distutils python3.10-venv \
      python3.11 python3.11-distutils python3.11-venv python3.12 python3.12-distutils python3.12-venv \
    && curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py \
    && python3.9 get-pip.py \
    && python3.10 get-pip.py \
    && python3.11 get-pip.py \
    && python3.12 get-pip.py \
    && rm get-pip.py \
    && ln -s "$(which python3.12)" /usr/bin/python

# Install YQ
RUN wget https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 -O /usr/bin/yq \
    && chmod +x /usr/bin/yq

# Install NodeJS 18
RUN cd /tmp \
    && curl --silent --location https://deb.nodesource.com/setup_18.x | bash - \
    && apt install -y nodejs

# Install AWS CLI
RUN curl -LO "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" \
    && unzip awscli-exe-linux-x86_64.zip \
    && ./aws/install \
    && rm -R aws awscli-exe-linux-x86_64.zip

# Install SAM CLI
RUN curl -LO "https://github.com/aws/aws-sam-cli/releases/latest/download/aws-sam-cli-linux-x86_64.zip" \
    && unzip aws-sam-cli-linux-x86_64.zip -d sam-installation \
    && ./sam-installation/install \
    && rm -R sam-installation aws-sam-cli-linux-x86_64.zip

# Install Terraform
RUN cd /tmp \
    && mkdir terraform-distributions \
    && cd terraform-distributions \
    && curl -LO "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip" \
    && curl -LO "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_SHA256SUMS" \
    && cat terraform_${TERRAFORM_VERSION}_SHA256SUMS | grep terraform_${TERRAFORM_VERSION}_linux_amd64.zip | sha256sum --check \
    && unzip "terraform_${TERRAFORM_VERSION}_linux_amd64.zip" -d /usr/local/bin/ \
    && chmod ugo+x /usr/local/bin/terraform \
    && cd /tmp \
    && rm -r terraform-distributions


# Install Atyos CLI \
COPY $ATYOSCLI_PATH .

RUN chmod +x atyos \
    && ./atyos cli/install
