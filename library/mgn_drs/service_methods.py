import logging
import os

from pydantic.error_wrappers import ValidationError

from library.aws_ressources.ec2 import get_ec2_launch_template, get_vpc, get_subnets, get_security_groups
from library.mgn_drs.instance_configuration.instance_configuration import InstanceConfiguration, ServiceConfiguration
from library.mgn_drs.instance_configuration.mappings import Mappings

logger = logging.getLogger("component.base")
logger.setLevel(logging.getLevelName(os.getenv("LOG_LEVEL", "INFO")))


# Client here is drs or mgn service_client
def get_source_server(service_client):
    # List the source servers for the DR environment
    response = service_client.describe_source_servers()

    if "items" in response:
        return response["items"]
    return None


def complete_source_servers(service_client, ec2_client, source_servers):
    for server in source_servers:
        launch_template = get_launch_template(service_client, ec2_client, server)
        if launch_template:
            server["launchTemplate"] = launch_template
    return source_servers


def get_launch_template(service_client, ec2_client, source_server):
    launch_template_id = get_launch_template_id(service_client, source_server.get('sourceServerID'))
    if launch_template_id is not None:
        template = get_ec2_launch_template(ec2_client, launch_template_id)
        return template

    return None


def get_launch_template_id(service_client, source_server_id):
    response = service_client.get_launch_configuration(
        sourceServerID=source_server_id
    )
    return response.get('ec2LaunchTemplateID')


def get_current_configuration(service_client, ec2_client):
    # get source servers
    source_servers = get_source_server(service_client)

    # Add launch_templates to source templates
    source_servers = complete_source_servers(service_client, ec2_client, source_servers)

    vpc_s = get_vpc(ec2_client)
    # Create an instance of the Pydantic model and perform data validation

    launch_templates = {}
    templates_name = {}
    for source_server in source_servers:
        name = source_server.get("tags").get("Name")
        try:
            launch_template_data = source_server.get("launchTemplate", {}).get("LaunchTemplateData", {})
            instance_configuration = InstanceConfiguration(**launch_template_data)
            launch_templates[name] = instance_configuration
            templates_name[name] = source_server.get("launchTemplate", {}).get("LaunchTemplateId")
        except ValidationError as e:
            errors = e.errors()
            error_messages = []
            for error in errors:
                error_messages.append(f"{error['loc']}: {error['msg']}")
            logger.error(f"{name} could not be loaded from AWS due to validation error:\n {error_messages}")
    vpc_ids = [x.get('VpcId') for x in vpc_s]
    subnets = get_subnets(ec2_client, vpc_ids)
    security_groups = get_security_groups(ec2_client, vpc_ids)
    mappings = Mappings(vpc_s=vpc_s, subnets=subnets, security_groups=security_groups, templates_name=templates_name)
    mgn_configuration = ServiceConfiguration(launch_templates=launch_templates, mappings=mappings)
    mgn_configuration.post_load()

    return mgn_configuration


def get_new_configuration(parameters, source_servers):
    launch_templates = {}
    for source_server_name, source_server in source_servers.items():
        try:
            instance_configuration = InstanceConfiguration(**source_server)
            launch_templates[source_server_name] = instance_configuration
        except ValidationError as e:
            errors = e.errors()
            error_messages = []
            for error in errors:
                error_messages.append(f"{error['loc']}: {error['msg']}")
            logger.error(f"{source_server_name} could not be loaded due to validation error:\n {error_messages}")

    mappings = Mappings(subnets=parameters.get("subnets"), security_groups=parameters.get("securityGroups"))

    service_configuration = ServiceConfiguration(launch_templates=launch_templates, mappings=mappings)
    service_configuration.post_load(parameters.get("tags"))

    return service_configuration
