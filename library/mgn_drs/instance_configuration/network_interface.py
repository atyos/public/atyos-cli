import ipaddress
from typing import List, Optional

from pydantic.class_validators import validator

from library.mgn_drs.instance_configuration.base_model_configuration import BaseModelConfiguration
from library.mgn_drs.instance_configuration.mappings import Mappings


class Ipv6Address(BaseModelConfiguration):
    Ipv6Address: str


class PrivateIpAddress(BaseModelConfiguration):
    Primary: "bool"
    PrivateIpAddress: str


class NetworkInterface(BaseModelConfiguration):
    AssociateCarrierIpAddress: Optional["bool"]
    AssociatePublicIpAddress: Optional["bool"]
    DeleteOnTermination: Optional["bool"]
    Description: Optional[str]
    DeviceIndex: int
    Groups: Optional[List[str]]
    InterfaceType: Optional[str]
    Ipv6AddressCount: Optional[int]
    Ipv6Addresses: Optional[List[Ipv6Address]]
    NetworkInterfaceId: Optional[str]
    PrivateIpAddress: Optional[str]
    PrivateIpAddresses: Optional[List[PrivateIpAddress]]
    SecondaryPrivateIpAddressCount: Optional[int]
    SubnetId: Optional[str]
    NetworkCardIndex: Optional[int]
    Ipv4Prefixes: Optional[List[str]]
    Ipv4PrefixCount: Optional[int]
    Ipv6Prefixes: Optional[List[str]]
    Ipv6PrefixCount: Optional[int]

    @validator("PrivateIpAddress")
    def validate_private_ip_address(value: str):
        if value not in ["CREATE NEW", "COPY SOURCE"]:
            if not NetworkInterface.is_ipv4(value):
                raise ValueError(f"Invalid Private IP Address: {value}")

    @staticmethod
    def is_ipv4(string):
        try:
            ipaddress.IPv4Network(string)
            return True
        except ValueError:
            return False

    def post_load(self, mappings: Mappings):
        if mappings.MappingSubnets and self.SubnetId:
            if self.SubnetId in mappings.MappingSubnets:
                self.SubnetId = mappings.MappingSubnets[self.SubnetId]
        if mappings.MappingSecurityGroups and self.Groups:
            groups = []
            for group in self.Groups:
                if group in mappings.MappingSecurityGroups:
                    groups.append(mappings.MappingSecurityGroups[group])
                else:
                    groups.append(group)
            self.Groups = groups

    def pre_save(self, mappings: Mappings):
        if mappings.Subnets and self.SubnetId:
            if self.SubnetId in mappings.Subnets:
                self.SubnetId = mappings.Subnets[self.SubnetId].get("id")
        if mappings.SecurityGroups and self.Groups:
            groups = []
            for group in self.Groups:
                if group in mappings.SecurityGroups:
                    groups.append(mappings.SecurityGroups[group])
                else:
                    groups.append(group)
            self.Groups = groups
