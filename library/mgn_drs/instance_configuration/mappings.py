from typing import Dict

from library.aws_ressources.ec2 import get_name


class Mappings:
    MappingSubnets: Dict
    MappingSecurityGroups: Dict
    Subnets: Dict
    SecurityGroups: Dict
    TemplatesName: Dict

    def __init__(self, **kwargs):
        mapping = {}
        mapping_subnets = {}
        mapping_security_groups = {}
        parameter_subnets = {}
        parameter_security_groups = {}

        templates_names = kwargs.get("templates_name", {})

        if vpc_s := kwargs.get("vpc_s"):
            for vpc in vpc_s:
                vpc_id = vpc.get('VpcId')
                tags = vpc.get('Tags')
                if tags is None:
                    continue
                for tag in tags:
                    if tag.get("Key") == "Name":
                        mapping[vpc_id] = tag.get("Value")
                        break
            subnets = kwargs.get("subnets")
            security_groups = kwargs.get("security_groups")
            for subnet in subnets:
                tags = subnet.get('Tags')
                if tags is None:
                    continue
                name = get_name(tags)
                vpc_id = subnet.get('VpcId')
                vpc_name = mapping.get(vpc_id)
                name = f"{vpc_name}-{name}"
                subnet_id = subnet.get('SubnetId')
                cidr = subnet.get('CidrBlock')
                parameter_subnets[name] = {
                    "id": subnet_id,
                    "cidr": cidr
                }
                mapping_subnets[subnet_id] = name

            for security_group in security_groups:
                name = security_group.get('GroupName')
                vpc_id = security_group.get('VpcId')
                vpc_name = mapping.get(vpc_id)
                name = f"{vpc_name}-{name}"
                sg_id = security_group.get('GroupId')
                parameter_security_groups[name] = sg_id
                mapping_security_groups[sg_id] = name
        else:
            parameter_subnets = kwargs.get("subnets")
            parameter_security_groups = kwargs.get("security_groups")
        self.MappingSubnets = mapping_subnets
        self.MappingSecurityGroups = mapping_security_groups
        self.Subnets = parameter_subnets
        self.SecurityGroups = parameter_security_groups
        self.TemplatesName = templates_names

    def update_mapping(self, subnets: dict = None, security_groups: dict = None):
        if subnets:
            for subnet_name, subnet_id in subnets.items():
                self.Subnets[subnet_name] = subnet_id
                self.MappingSubnets[subnet_id.get("id")] = subnet_name
        if security_groups:
            for sg_name, sg_id in security_groups.items():
                self.SecurityGroups[sg_name] = sg_id
                self.MappingSecurityGroups[sg_id] = sg_name
