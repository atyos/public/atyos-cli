from typing import Optional

from pydantic.class_validators import validator
from pydantic.fields import Field

from library.mgn_drs.instance_configuration.base_model_configuration import BaseModelConfiguration


class Ebs(BaseModelConfiguration):
    Encrypted: Optional["bool"] = Field(default=True)
    DeleteOnTermination: Optional["bool"] = Field(default=True)
    Iops: Optional[int] = Field(default="3000")
    KmsKeyId: Optional[str]
    SnapshotId: Optional[str]
    VolumeSize: Optional[int]
    VolumeType: Optional[str] = Field(default="gp3")
    Throughput: Optional[int] = Field(default="150")

    @validator("VolumeType")
    def validate_volume_type(value: str):
        if value not in ["standard", "gp2", "gp3", "io1", "io2", "st1", "sc1"]:
            raise ValueError(f"Invalid Volume Type: {value}")
        return value


class BlockDeviceMapping(BaseModelConfiguration):
    DeviceName: str
    VirtualName: Optional[str]
    Ebs: Ebs
    NoDevice: Optional[str] = None
