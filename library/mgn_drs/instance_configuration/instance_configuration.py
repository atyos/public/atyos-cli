import os
import sys
from datetime import datetime
from typing import List, Optional, Dict

import yaml
from pydantic import BaseModel, Field
from pydantic.class_validators import validator

from library.mgn_drs.instance_configuration.base_model_configuration import BaseModelConfiguration
from library.mgn_drs.instance_configuration.block_device_mapping import BlockDeviceMapping
from library.mgn_drs.instance_configuration.instance_requirements import InstanceRequirements
from library.mgn_drs.instance_configuration.mappings import Mappings
from library.mgn_drs.instance_configuration.network_interface import NetworkInterface

yaml_file_path = os.path.join(sys._MEIPASS, 'library', 'mgn_drs', 'instance_configuration', 'conf.yaml')
# yaml_file_path = "/home/louis/Git/ATYOS/atyos-cli/library/mgn_drs/instance_configuration/conf.yaml"

with open(yaml_file_path, 'r') as file:
    conf = yaml.safe_load(file)


class IamInstanceProfile(BaseModelConfiguration):
    Arn: Optional[str]
    Name: Optional[str]


class Monitoring(BaseModelConfiguration):
    Enabled: Optional[bool]


class Placement(BaseModelConfiguration):
    AvailabilityZone: Optional[str]
    Affinity: Optional[str]
    GroupName: Optional[str]
    HostId: Optional[str]
    Tenancy: Optional[str]
    SpreadDomain: Optional[str]
    HostResourceGroupArn: Optional[str]
    PartitionNumber: Optional[int]
    GroupId: Optional[str]

    @validator("Tenancy")
    def validate_tenancy(value: str):
        if value not in ["default", "dedicated", "host"]:
            raise ValueError(f"Invalid Tenancy: {value}")
        return value


class Tag(BaseModelConfiguration):
    Key: Optional[str]
    Value: Optional[str]


class TagSpecification(BaseModelConfiguration):
    ResourceType: Optional[str]
    Tags: Optional[List[Tag]]

    @staticmethod
    def update_list_fields(list_instance: List['TagSpecification'], other_instance: List['TagSpecification'],
                           id_field: str, no_remove=False):
        if len(other_instance) > 0:
            tags = other_instance[0].Tags
        else:
            return list_instance
        if len(list_instance) == 0:
            list_instance.append(TagSpecification(ResourceType='instance', Tags=[]))

        mandatory_resource_type = conf.get("tags_resource_type")
        resource_types = [x.ResourceType for x in list_instance]
        for resource_type in mandatory_resource_type:
            if resource_type not in resource_types:
                list_instance.append(TagSpecification(ResourceType=resource_type, Tags=list_instance[0].Tags))

        for tag_specification in list_instance:
            tag_specification.Tags = Tag.update_list_fields(tag_specification.Tags, tags, id_field, no_remove)

        return list_instance

    @staticmethod
    def compare_list_fields(list_instance: List['TagSpecification'], other_instance: List['TagSpecification'],
                            id_field: str):
        diff = {"current_template": [], "new_template": []}

        if len(other_instance) > 0:
            tags = other_instance[0].Tags
        else:
            return None

        for tag_specification in list_instance:
            local_diff = Tag.compare_list_fields(tag_specification.Tags, tags, id_field)
            if local_diff:
                diff["current_template"].append(local_diff.get("current_template"))
                diff["new_template"].append(local_diff.get("new_template"))

        if diff == {"current_template": [], "new_template": []}:
            return None
        return diff

    def post_load(self, tags: List[Tag]):
        self.Tags = Tag.update_list_fields(tags, self.Tags, "Key", no_remove=True)


class SpotOptions(BaseModelConfiguration):
    MaxPrice: Optional[str]
    SpotInstanceType: Optional[str]
    BlockDurationMinutes: Optional[int]
    ValidUntil: Optional[datetime]
    InstanceInterruptionBehavior: Optional[str]

    @validator("InstanceInterruptionBehavior")
    def validate_instance_interruption_behavior(value: str):
        if value not in ["stop", "terminate", "hibernate"]:
            raise ValueError(f"Invalid Instance Interruption Behavior: {value}")
        return value

    @validator("SpotInstanceType")
    def validate_spot_instance_type(value: str):
        if value not in ["one-time", "persistent"]:
            raise ValueError(f"Invalid Spot Instance Type: {value}")
        return value


class InstanceMarketOptions(BaseModelConfiguration):
    MarketType: Optional[str]
    SpotOptions: Optional[SpotOptions]


class CapacityReservationTarget(BaseModelConfiguration):
    CapacityReservationId: Optional[str]
    CapacityReservationResourceGroupArn: Optional[str]


class CapacityReservationSpecification(BaseModelConfiguration):
    CapacityReservationPreference: Optional[str]
    CapacityReservationTarget: Optional[CapacityReservationTarget]

    @validator("CapacityReservationPreference")
    def validate_capacity_reservation_preference(value: str):
        if value not in ["open", "none"]:
            raise ValueError(f"Invalid Capacity Reservation Preference: {value}")
        return value


class LicenseSpecification(BaseModelConfiguration):
    LicenseConfigurationArn: Optional[str]


class CpuOptions(BaseModelConfiguration):
    CoreCount: Optional[int]
    ThreadsPerCore: Optional[int]
    AmdSevSnp: Optional[str]

    @validator("AmdSevSnp")
    def validate_amd_sev_snp(value: str):
        if value not in ["enabled", "disabled"]:
            raise ValueError(f"Invalid AmdSevSnp: {value}")
        return value


class ElasticGpuSpecifications(BaseModel):
    Type: str


class ElasticInferenceAccelerator(BaseModel):
    Type: str
    Count: int


class CreditSpecification(BaseModel):
    CpuCredits: str


class HibernationOptions(BaseModel):
    Configured: bool


class MetadataOptions(BaseModel):
    HttpTokens: str
    HttpPutResponseHopLimit: int
    HttpEndpoint: str
    HttpProtocolIpv6: str
    InstanceMetadataTags: str

    # write validator for all fields
    @validator("InstanceMetadataTags")
    def validate_instance_metadata_tags(value: str):
        if value not in ["disable", "enable"]:
            raise ValueError(f"Invalid Instance Metadata Tags: {value}")
        return value

    @validator("HttpProtocolIpv6")
    def validate_http_protocol_ipv6(value: str):
        if value not in ["disable", "enable"]:
            raise ValueError(f"Invalid Http Protocol Ipv6: {value}")
        return value

    @validator("HttpEndpoint")
    def validate_http_endpoint(value: str):
        if value not in ["disable", "enable"]:
            raise ValueError(f"Invalid Http Endpoint: {value}")
        return value

    @validator("HttpTokens")
    def validate_http_tokens(value: str):
        if value not in ["optional", "required"]:
            raise ValueError(f"Invalid Http Tokens: {value}")
        return value


class EnclaveOptions(BaseModel):
    Enabled: bool


class PrivateDnsNameOptions(BaseModelConfiguration):
    HostnameType: Optional[str]
    EnableResourceNameDnsARecord: Optional[bool]
    EnableResourceNameDnsAAAARecord: Optional[bool]

    @validator("HostnameType")
    def validate_hostname_type(value: str):
        if value not in ["ip-name", "resource-name"]:
            raise ValueError(f"Invalid HostnameType: {value}")
        return value


class MaintenanceOptions(BaseModelConfiguration):
    AutoRecovery: Optional[str]

    @validator("AutoRecovery")
    def validate_auto_recovery(value: str):
        if value not in ["default", "disabled"]:
            raise ValueError(f"Invalid AutoRecovery: {value}")
        return value


class InstanceConfiguration(BaseModelConfiguration):
    Ignore: Optional["bool"] = Field(default=False)
    KernelId: Optional[str]
    EbsOptimized: Optional["bool"]
    IamInstanceProfile: Optional[IamInstanceProfile]
    BlockDeviceMappings: Optional[List[BlockDeviceMapping]]
    NetworkInterfaces: Optional[List[NetworkInterface]]
    ImageId: Optional[str]
    InstanceType: Optional[str]
    KeyName: Optional[str]
    Monitoring: Optional[Monitoring]
    Placement: Optional[Placement]
    RamDiskId: Optional[str]
    DisableApiTermination: Optional[bool]
    InstanceInitiatedShutdownBehavior: Optional[str]
    UserData: Optional[str]
    TagSpecifications: Optional[List[TagSpecification]]
    ElasticGpuSpecifications: Optional[List[ElasticGpuSpecifications]]
    ElasticInferenceAccelerators: Optional[List[ElasticInferenceAccelerator]]
    SecurityGroupIds: Optional[List[str]]
    SecurityGroups: Optional[List[str]]
    InstanceMarketOptions: Optional[InstanceMarketOptions]
    CreditSpecification: Optional[CreditSpecification]
    CpuOptions: Optional[CpuOptions]
    CapacityReservationSpecification: Optional[CapacityReservationSpecification]
    LicenseSpecifications: Optional[List[LicenseSpecification]]
    HibernationOptions: Optional[HibernationOptions]
    MetadataOptions: Optional[MetadataOptions]
    EnclaveOptions: Optional[EnclaveOptions]
    InstanceRequirements: Optional[InstanceRequirements]
    PrivateDnsNameOptions: Optional[PrivateDnsNameOptions]
    MaintenanceOptions: Optional[MaintenanceOptions]
    DisableApiStop: Optional[bool]

    @validator("InstanceType")
    def validate_instance_type(value: str):
        instance_type_values = conf.get("instance_types")
        if value not in instance_type_values:
            raise ValueError(f"Invalid instance type: {value}")
        return value

    @validator('InstanceInitiatedShutdownBehavior')
    def validate_instance_type_shutdown_behavior(value):
        if value not in ['stop', 'terminate']:
            raise ValueError(f"Invalid instance type: {value}")
        return value

    def post_load(self, global_tags: List[Tag] = None, mappings: Mappings = None):
        if global_tags and self.TagSpecifications:
            for tag_specification in self.TagSpecifications:
                tag_specification.post_load(global_tags)
        if mappings and self.NetworkInterfaces:
            for network_interface in self.NetworkInterfaces:
                network_interface.post_load(mappings)

    def pre_save(self, mappings: Mappings):
        if mappings and self.NetworkInterfaces:
            for network_interface in self.NetworkInterfaces:
                network_interface.pre_save(mappings)
        # if we don't want to Ignore this template we remove the field from dict
        if not self.Ignore:
            self.Ignore = None


class ServiceConfiguration:
    LaunchTemplates: Dict[str, InstanceConfiguration]
    Mappings: Mappings

    def __init__(self, launch_templates: Dict[str, InstanceConfiguration], mappings: Mappings):
        self.LaunchTemplates = launch_templates
        self.Mappings = mappings

    def export(self, show_default=False):
        state_as_dict = {
            "parameters": {"subnets": self.Mappings.Subnets, "securityGroups": self.Mappings.SecurityGroups},
            "launchTemplates": {}
        }
        if show_default:
            state_as_dict["launchTemplates"]["default"] = conf.get("default_launch_template")
        for name, template in self.LaunchTemplates.items():
            state_as_dict["launchTemplates"][name] = template.dict(export=True, exclude_none=True)
        return state_as_dict

    def post_load(self, global_tags: List = None):
        kwargs = {"mappings": self.Mappings}
        if global_tags:
            tags = []
            for tag in global_tags:
                tags.append(Tag(Key=tag.get('Key'), Value=tag.get('Value')))
            kwargs["global_tags"] = tags
        for template in self.LaunchTemplates.values():
            template.post_load(**kwargs)

    def pre_save(self):
        for template in self.LaunchTemplates.values():
            template.pre_save(self.Mappings)
