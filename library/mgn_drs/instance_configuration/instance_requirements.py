from typing import List, Optional

from pydantic.class_validators import validator

from library.mgn_drs.instance_configuration.base_model_configuration import BaseModelConfiguration


class MinMaxFloat(BaseModelConfiguration):
    Min: float
    Max: float


class MinMaxInt(BaseModelConfiguration):
    Min: int
    Max: int


class InstanceRequirements(BaseModelConfiguration):
    VCpuCount: Optional[MinMaxInt]
    MemoryMiB: Optional[MinMaxInt]
    CpuManufacturers: Optional[List[str]]
    MemoryGiBPerVCpu: Optional[MinMaxFloat]
    ExcludedInstanceTypes: Optional[List[str]]
    InstanceGenerations: Optional[List[str]]
    SpotMaxPricePercentageOverLowestPrice: Optional[int]
    OnDemandMaxPricePercentageOverLowestPrice: Optional[int]
    BareMetal: Optional[str]
    BurstablePerformance: Optional[str]
    RequireHibernateSupport: Optional[bool]
    NetworkInterfaceCount: Optional[MinMaxInt]
    LocalStorage: Optional[str]
    LocalStorageTypes: Optional[List[str]]
    TotalLocalStorageGB: Optional[MinMaxFloat]
    BaselineEbsBandwidthMbps: Optional[MinMaxInt]
    AcceleratorTypes: Optional[List[str]]
    AcceleratorCount: Optional[MinMaxInt]
    AcceleratorManufacturers: Optional[List[str]]
    AcceleratorNames: Optional[List[str]]
    AcceleratorTotalMemoryMiB: Optional[MinMaxInt]
    NetworkBandwidthGbps: Optional[MinMaxFloat]
    AllowedInstanceTypes: Optional[List[str]]

    @validator("BareMetal")
    def validate_bare_metal(value: str):
        if value not in ["included", "excluded", "required"]:
            raise ValueError(f"Invalid BareMetal: {value}")
        return value

    @validator("BurstablePerformance")
    def validate_burstable_performance(value: str):
        if value not in ["included", "excluded", "required"]:
            raise ValueError(f"Invalid BurstablePerformance: {value}")
        return value

    @validator("LocalStorage")
    def validate_local_storage(value: str):
        if value not in ["included", "excluded", "required"]:
            raise ValueError(f"Invalid LocalStorage: {value}")
        return value
