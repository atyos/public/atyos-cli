from typing import List, Dict, Any

from pydantic import BaseModel


class BaseModelConfiguration(BaseModel):
    def dict(self, export: bool = False, *args, **kwargs) -> Dict[str, Any]:
        # Call the parent class's dict() method
        base_dict = super().dict(*args, **kwargs)
        if export:
            # In the case of export, hide the fields that are not needed
            if "NetworkInterfaces" in base_dict:
                network_interfaces = []
                for network_interface in base_dict["NetworkInterfaces"]:
                    if "Ipv6AddressCount" in network_interface:
                        del network_interface["Ipv6AddressCount"]
                    if "Ipv6Addresses" in network_interface:
                        del network_interface["Ipv6Addresses"]
                    if "SecondaryPrivateIpAddressCount" in network_interface:
                        del network_interface["SecondaryPrivateIpAddressCount"]
                    if "Ipv4Prefixes" in network_interface:
                        del network_interface["Ipv4Prefixes"]
                    if "Ipv4PrefixCount" in network_interface:
                        del network_interface["Ipv4PrefixCount"]
                    if "Ipv6Prefixes" in network_interface:
                        del network_interface["Ipv6Prefixes"]
                    if "Ipv6PrefixCount" in network_interface:
                        del network_interface["Ipv6PrefixCount"]
                    network_interfaces.append(network_interface)
                base_dict["NetworkInterfaces"] = network_interfaces
            # only Show Tags of first list
            if "TagSpecifications" in base_dict:
                tag_specifications = []
                if len(base_dict["TagSpecifications"]) > 0:
                    tags = base_dict["TagSpecifications"][0]["Tags"]
                    tag_specifications.append({"Tags": tags})
                base_dict["TagSpecifications"] = tag_specifications
        return base_dict

    def update_fields(self, other_instance: 'BaseModelConfiguration', no_remove=False):
        from library.mgn_drs.instance_configuration.instance_configuration import BlockDeviceMapping, NetworkInterface, \
            TagSpecification
        for key, value in other_instance.__dict__.items():
            if value is None:
                continue
            if isinstance(value, List) and len(value) > 0:
                field_class = value[0]
                if isinstance(field_class, BlockDeviceMapping):
                    new_data = BlockDeviceMapping.update_list_fields(getattr(self, key), value, "DeviceName", no_remove)
                    setattr(self, key, new_data)
                elif isinstance(field_class, NetworkInterface):
                    new_data = NetworkInterface.update_list_fields(getattr(self, key), value, "DeviceIndex", no_remove)
                    setattr(self, key, new_data)
                elif isinstance(field_class, TagSpecification):
                    new_data = TagSpecification.update_list_fields(getattr(self, key), value, "Key", no_remove)
                    setattr(self, key, new_data)
                else:
                    setattr(self, key, value)
            elif isinstance(value, BaseModelConfiguration):
                getattr(self, key).update_fields(value)
                setattr(self, key, getattr(self, key))
            else:
                # If PrivateIpAddress is CREATE_NEW, we don't send IP and AWS create an IP
                # If PrivateIpAddress is COPY_SOURCE we put source server's IP
                if key == "PrivateIpAddress":
                    if value == "CREATE NEW":
                        setattr(self, key, None)
                    elif value == "COPY SOURCE":
                        continue
                    else:
                        setattr(self, key, value)
                else:
                    setattr(self, key, value)

    def compare_with(self, other_instance: 'BaseModelConfiguration'):
        from library.mgn_drs.instance_configuration.instance_configuration import BlockDeviceMapping, NetworkInterface, \
            TagSpecification
        diff = {"current_template": {}, "new_template": {}}
        for key, value in other_instance.__dict__.items():
            if value is None:
                continue
            if isinstance(value, List) and len(value) > 0:
                field_class = value[0]
                if isinstance(field_class, BlockDeviceMapping):
                    local_diff = BlockDeviceMapping.compare_list_fields(getattr(self, key), value, "DeviceName")
                    if local_diff:
                        diff["current_template"][key] = local_diff.get("current_template")
                        diff["new_template"][key] = local_diff.get("new_template")
                elif isinstance(field_class, NetworkInterface):
                    local_diff = NetworkInterface.compare_list_fields(getattr(self, key), value, "DeviceIndex")
                    if local_diff:
                        diff["current_template"][key] = local_diff.get("current_template")
                        diff["new_template"][key] = local_diff.get("new_template")
                elif isinstance(field_class, TagSpecification):
                    local_diff = TagSpecification.compare_list_fields(getattr(self, key), value, "Key")
                    if local_diff:
                        diff["current_template"][key] = local_diff.get("current_template")
                        diff["new_template"][key] = local_diff.get("new_template")
                else:
                    if getattr(self, key) != value:
                        diff["current_template"][key] = getattr(self, key)
                        diff["new_template"][key] = value
            elif isinstance(value, BaseModelConfiguration):
                local_diff = getattr(self, key).compare_with(value)
                if local_diff:
                    diff["current_template"][key] = local_diff.get("current_template")
                    diff["new_template"][key] = local_diff.get("new_template")
            else:
                if getattr(self, key) != value:
                    diff["current_template"][key] = getattr(self, key)
                    diff["new_template"][key] = value
        if diff == {"current_template": {}, "new_template": {}}:
            return None
        return diff

    @staticmethod
    def update_list_fields(list_instance: List, other_instance: List, id_field: str, no_remove=False):
        dict_instance = {getattr(x, id_field): x for x in list_instance}
        dict_other_instance = {getattr(x, id_field): x for x in other_instance}
        new_list_instance = []
        for key, value in dict_other_instance.items():
            if key in dict_instance:
                dict_instance[key].update_fields(value)
                new_list_instance.append(dict_instance[key])
            else:
                new_list_instance.append(value)
        if no_remove:
            for key, value in dict_instance.items():
                if key not in dict_other_instance:
                    new_list_instance.append(value)
        return new_list_instance

    @staticmethod
    def compare_list_fields(list_instance: List, other_instance: List, id_field: str):
        dict_instance = {getattr(x, id_field): x for x in list_instance}
        dict_other_instance = {getattr(x, id_field): x for x in other_instance}
        diff = {"current_template": [], "new_template": []}
        for key, value in dict_other_instance.items():
            if key in dict_instance:
                local_diff = dict_instance[key].compare_with(value)
                if local_diff:
                    diff["current_template"].append(local_diff.get("current_template"))
                    diff["new_template"].append(local_diff.get("new_template"))
            else:
                diff["new_template"].append(value)
        for key, value in dict_instance.items():
            if key in dict_other_instance:
                continue
            diff["current_template"].append(value)
        if diff == {"current_template": [], "new_template": []}:
            return None
        return diff
