from typing import List


def get_ec2_launch_template(ec2_client, ec2_launch_template_id):
    response = ec2_client.describe_launch_template_versions(LaunchTemplateId=ec2_launch_template_id,
                                                            Versions=["$Default"])

    if "LaunchTemplateVersions" in response:
        if len(response["LaunchTemplateVersions"]) > 0:
            return response["LaunchTemplateVersions"][0]
    return None


def get_vpc(ec2_client, vpc_ids: List = None):
    # Call the describe_vpcs operation to get a list of VPCs
    params = {}
    if vpc_ids:
        params['Filters'] = [
            {
                'Name': 'vpc-id',
                'Values': vpc_ids
            }
        ]
    response = ec2_client.describe_vpcs(**params)

    if "Vpcs" in response:
        return response["Vpcs"]
    return None


def get_subnets(ec2_client, vpc_ids: List):
    # Set up the parameters for the describe_subnets operation
    params = {
        'Filters': [
            {
                'Name': 'vpc-id',
                'Values': vpc_ids
            }
        ]
    }

    # Call the describe_subnets operation to get a list of subnets
    response = ec2_client.describe_subnets(**params)

    if "Subnets" in response:
        return response["Subnets"]
    return None


def get_security_groups(ec2_client, vpc_ids: List):
    # Set up the parameters for the describe_security_groups operation
    params = {
        'Filters': [
            {
                'Name': 'vpc-id',
                'Values': vpc_ids
            }
        ]
    }

    # Call the describe_security_groups operation to get a list of security groups
    response = ec2_client.describe_security_groups(**params)

    if "SecurityGroups" in response:
        return response["SecurityGroups"]
    return None


def update_default_launch_template(ec2_client, ec2_launch_template_id, default_version):
    response = ec2_client.modify_launch_template(
        DefaultVersion=default_version,
        LaunchTemplateId=ec2_launch_template_id,
    )
    return response


def create_launch_template(ec2_client, launch_template_id, launch_template_name, launch_template_data):
    instance = ec2_client.create_launch_template_version(
        LaunchTemplateId=launch_template_id,
        VersionDescription=launch_template_name,
        LaunchTemplateData=launch_template_data,
    )
    return instance


def create_tags(ec2_client, resource_id, tags):
    ec2_client.create_tags(
        Resources=[resource_id],
        Tags=tags
    )


def get_name(tags: List):
    for tag in tags:
        if tag['Key'] == 'Name':
            return tag['Value']
    return None
