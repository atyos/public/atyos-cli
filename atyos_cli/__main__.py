import configargparse
import logging
import os
import sys
from typing import Optional, List
from contextlib import redirect_stderr
from app import App
from components_base import ComponentConfigMode, ComponentError

if __name__ == '__main__':
    ROOT_PATH = os.path.realpath(os.path.dirname(os.path.abspath(__file__)))
else:
    ROOT_PATH = os.path.realpath(os.path.dirname(os.path.abspath(__file__)) + "/..")

CORE_COMPONENTS_DIR = f"{ROOT_PATH}/components"
COMPONENTS_PATHS = ":".join((CORE_COMPONENTS_DIR, "~/.atyos_cli/custom", os.getenv("ATYOS_CLI_PATH", "")))

# Setup logging
logger = logging.getLogger()
loglevel = logging.getLevelName(os.getenv("LOG_LEVEL", "INFO"))
logger.setLevel(loglevel)
ch = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

#
logger.info("atyos_cli - A DevOps utility tool")
logger.debug(f"Application root: {ROOT_PATH}")
logger.debug(f"Components paths: {COMPONENTS_PATHS}")

# Get arguments from CLI
parser = configargparse.ArgParser("atyos_cli", description="Devops utility tools")
parser.add_argument(f"component", help="Component to execute")

try:
    with redirect_stderr(None):
        arguments, other_args = parser.parse_known_args(ignore_help_args=True)
except BaseException as e:
    arguments, other_args = parser.parse_known_args()


def include_paths(path_string: Optional[str]) -> List[str]:
    components_paths = []
    if path_string is not None:
        extra_paths = path_string.split(":")
        for extra_path in extra_paths:
            extra_path = os.path.abspath(os.path.expanduser(extra_path))
            if os.path.isdir(extra_path):
                logger.debug(f"Components includes: Adding {extra_path} to PATHS")
                components_paths.append(extra_path)
                sys.path.append(extra_path)

    return components_paths


logger.info(f"Loading component: {arguments.component}")
include_paths(COMPONENTS_PATHS)
logger.debug(f"Python path: {sys.path}")
app = App(
    arguments.component,
    component_config_mode=ComponentConfigMode.CLI,
    cli_parser=parser,
)

logger.info(f"Successfully loaded component: {arguments.component}")

try:
    app.run()
except ComponentError as e:
    logger.error(e)
    sys.exit(1)


print("\n == AT YOur Service ==")
