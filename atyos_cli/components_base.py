import abc
import logging
import os
from enum import Enum

import boto3
from botocore.exceptions import ClientError, ParamValidationError
from rich.console import Console

logger = logging.getLogger("component.base")
logger.setLevel(logging.getLevelName(os.getenv("LOG_LEVEL", "INFO")))


class ComponentConfigMode(Enum):
    CLI = 1
    FILE = 2


class Component(metaclass=abc.ABCMeta):

    def __init__(self, app: "App", configuration):
        self.app = app
        self.configuration = configuration
        self.cli_args = []
        self.console = Console()

    @abc.abstractmethod
    def run(self):
        ...

    def _parse_cli_args(self):
        parser = self.app.cli_parser
        args_group = parser.add_argument_group(self.app.component_name)

        for kwargs in self.cli_args:
            if flag := kwargs.pop("flag", None):
                args = (kwargs.pop("name"), flag)
            else:
                args = (kwargs.pop("name"), )

            args_group.add_argument(*args, **kwargs)

        self.configuration = vars(parser.parse_args())


class AwsComponent(Component, metaclass=abc.ABCMeta):

    def __init__(self, app: "App", configuration):
        super().__init__(app, configuration)
        self.boto = None

        self.cli_args.extend([
            {"name": "--profile", "help": "AWS profile to use to execute deployment", "required": False},
            {"name": "--region", "help": "AWS region to use to execute this command", "required": False},
            {"name": "--assume-role", "action": "append", "required": False,
             "help": "IAM Role to assume during execution, can be used multiple times to describe role chaining"}
        ])

    def _aws_init(self):
        self._assume_role()

    def assume_web_identity_role(self, role_arn, token, session_name="atyoscli_session", session_duration=3600):
        session = self._boto_session()
        sts_client = session.client("sts")
        try:
            sts_response = sts_client.assume_role_with_web_identity(
                RoleArn=role_arn,
                RoleSessionName=session_name,
                DurationSeconds=session_duration,
                WebIdentityToken=token
            )
        except (ClientError, ParamValidationError) as e:
            raise AwsComponentError(str(e))

        return sts_response.get("Credentials")

    def assume_role(self, role_arn, session_name="atyoscli_session", session_duration=3600):
        session = self._boto_session()
        sts_client = session.client("sts")
        try:
            sts_response = sts_client.assume_role(
                RoleArn=role_arn,
                RoleSessionName=session_name,
                DurationSeconds=session_duration
            )
        except (ClientError, ParamValidationError) as e:
            raise AwsComponentError(str(e))

        return sts_response.get("Credentials")

    def _assume_role(self):
        session = self._boto_session()

        assume_role = self.configuration.get("assume_role", [])
        if not type(assume_role) == list:
            assume_role = [assume_role]

        for role in assume_role:
            if not role:
                continue

            session_duration = self.configuration.get("assume_role_duration", 3600)
            session_name = self.configuration.get("assume_role_session_name", "atyoscli_session")

            try:
                if role.startswith("web-identity;"):
                    role_split = role.split(";")
                    role_arn = role_split[1]
                    token = role_split[2]

                    logger.info(f"Assuming web-identity-role {role_arn}")
                    credentials = self.assume_web_identity_role(
                        role_arn=role_arn,
                        token=token,
                        session_name=session_name,
                        session_duration=session_duration
                    )
                else:
                    logger.info(f"Assuming role {role}")
                    credentials = self.assume_role(
                        role_arn=role,
                        session_name=session_name,
                        session_duration=session_duration
                    )
            except ClientError as e:
                raise AwsComponentError(str(e))

            if credentials:
                os.environ["AWS_ACCESS_KEY_ID"] = credentials.get("AccessKeyId")
                os.environ["AWS_SECRET_ACCESS_KEY"] = credentials.get("SecretAccessKey")
                os.environ["AWS_SESSION_TOKEN"] = credentials.get("SessionToken")
                os.environ["AWS_DEFAULT_REGION"] = session.region_name
                session = boto3.session.Session(
                    aws_access_key_id=credentials.get("AccessKeyId"),
                    aws_secret_access_key=credentials.get("SecretAccessKey"),
                    aws_session_token=credentials.get("SessionToken"),
                    region_name=session.region_name
                )

        self.boto = session

    def _boto_session(self):
        if self.boto is not None:
            return self.boto

        aws_credentials = {}

        if self.configuration.get("profile"):
            aws_credentials = {"profile_name": self.configuration.get("profile")}
        if self.configuration.get("region"):
            aws_credentials["region_name"] = self.configuration.get("region")

        self.boto = boto3.session.Session(**aws_credentials)

        return self.boto


class ComponentError(Exception):
    ...


class AwsComponentError(ComponentError):
    ...
