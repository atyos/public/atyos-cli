import argparse
import importlib
import os.path
import sys

from components_base import ComponentConfigMode


class App:

    def __init__(self,
                 component_name,
                 component_config=None,
                 component_config_mode=ComponentConfigMode.CLI,
                 cli_parser: argparse.ArgumentParser = None):

        self.component_name = component_name
        self.component_config = component_config
        self.component_config_mode = component_config_mode

        self.cli_parser = cli_parser

        self.component_class: type = self.resolve_component(self.component_name)
        self.component = self.init_component()

    def get_class_name(self):
        name = self.component_name.split("/")[-1]
        return "".join(map(lambda w: w.capitalize(), name.split("_"))) + "Component"

    def resolve_component(self, component_name: str) -> type:
        component_module_path = f"{component_name}".replace("/", ".").replace("-", "_")

        # Add component dir to path to allow custom dependencies
        module_path = importlib.util.find_spec(component_module_path).origin
        sys.path.append(os.path.dirname(module_path))

        component_module = importlib.import_module(component_module_path)
        class_name = self.get_class_name()

        if not hasattr(component_module, class_name):
            raise ModuleNotFoundError(f"Class {class_name} not found in {self.component_name}")

        return getattr(component_module, class_name)

    def init_component(self):
        return self.component_class(self, self.component_config)

    def run(self):
        if not hasattr(self.component, "run"):
            raise TypeError(f"Unable to run component {self.component_name}: No run function found")

        self.component.run()
