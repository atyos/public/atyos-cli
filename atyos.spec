# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(['atyos_cli/__main__.py'],
             pathex=['./venv/lib/python3.9/site-packages', './venv/lib/python3.10/site-packages', './venv/lib/python3.11/site-packages', './venv/lib/python3.12/site-packages'],
             binaries=[],
             datas=[
                ("components", "./components"),
                ("library", "./library"),
                ("library/mgn_drs/instance_configuration/conf.yaml", "conf.yaml")
             ],
             hiddenimports=[
                'components.version.bump',
                'components.sam_templates.deploy',
                'components.cli.install',
                'components.auth.assume',
                'rich.layout',
                'rich.prompt',
                'yaml',
                'git',
                'gitlab',
                'configparser',
                'semver',
                'configargparse',
                'pydantic',
                'rich.progress',
                'requests'
             ],
             hookspath=[],
             hooksconfig={},
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)

pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='atyos',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True,
          disable_windowed_traceback=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None )
