#!/usr/bin/env python
import logging
import os
import sys

from components_base import AwsComponent, ComponentConfigMode

logger = logging.getLogger("auth.assume")
logger.setLevel(logging.getLevelName(os.getenv("LOG_LEVEL", "INFO")))


class AssumeComponent(AwsComponent):
    def __init__(self, app: "App", configuration):
        super(AssumeComponent, self).__init__(app, configuration)

        logger.info("Initializing Authentication assume component")

        self.cli_args.extend(
            [
                {
                    "name": "--role-arn",
                    "flag": "-r",
                    "help": "ARN of the role to assume",
                    "required": True,
                },
                {
                    "name": "--oidc-token",
                    "flag": "-t",
                    "help": "OIDC token to assume a web-identity",
                    "required": False,
                },
                {
                    "name": "--output-file",
                    "flag": "-o",
                    "help": "Output file path",
                    "default": "/tmp/aws-assume.env",
                    "required": False,
                },
            ]
        )

        if app.component_config_mode is ComponentConfigMode.CLI:
            self._parse_cli_args()
        else:
            logger.error("Module can only be used with CLI config mode")
            sys.exit(1)

    def assume(self):
        role_arn = self.configuration.get("role_arn")
        oidc_token = self.configuration.get("oidc_token")

        if not role_arn:
            raise RuntimeError("role_arn is required")

        if oidc_token is None:
            logger.info(f"Assuming role {role_arn}")
            credentials = self.assume_role(role_arn)
        else:
            logger.info(f"Assuming role {role_arn} with OIDC token")
            credentials = self.assume_web_identity_role(role_arn, oidc_token)

        if not credentials:
            raise RuntimeError("Unable to assume role")

        with open(self.configuration.get("output_file"), "w") as f:
            f.write(f"AWS_ACCESS_KEY_ID={credentials['AccessKeyId']}\n")
            f.write(f"AWS_SECRET_ACCESS_KEY={credentials['SecretAccessKey']}\n")
            f.write(f"AWS_SESSION_TOKEN={credentials['SessionToken']}\n")

    def run(self):
        self._aws_init()
        self.assume()
        logger.info(
            f"You can now source .env file by running: "
            f"export $(grep -v '^#' {self.configuration.get('output_file')} | xargs)"
        )
