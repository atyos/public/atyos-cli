#!/usr/bin/env python
import functools
import logging
import os
import re
import shutil
import subprocess
import sys
import tempfile
import zipfile

import requests
from rich.progress import Progress, SpinnerColumn, DownloadColumn, TransferSpeedColumn

from components_base import ComponentConfigMode, AwsComponent

logger = logging.getLogger("tools.import_value")
logger.setLevel(logging.getLevelName(os.getenv("LOG_LEVEL", "INFO")))

ARN_PATTERN = r"^arn:[a-zA-Z0-9-]+:lambda:[a-zA-Z0-9-]+:\d{12}:layer:[a-zA-Z0-9-_]+:\d+$"
"""
Regex patter from aws documentation for the arn parameter
see: https://docs.aws.amazon.com/lambda/latest/dg/API_GetLayerVersionByArn.html
"""
CHUNK_SIZE = 10_000
POSSIBLE_PYTHON_BIN_NAMES = [
    "python",
    "python3",
    "python3.11",
    "python3.12",
    "python3.10"
]


def _check_is_python_cmd_present():
    if not shutil.which("python"):
        logger.error(
            "Can't find a python version (looking for executable \'python\')"
        )
        sys.exit(1)


@functools.cache
def _get_python_bin():
    found_python_name = None

    for python_bin_name in POSSIBLE_PYTHON_BIN_NAMES:
        if shutil.which(python_bin_name):
            found_python_name = python_bin_name
            break

    if found_python_name:
        return found_python_name

    for i in range(1, 20):
        python_bin_name = f"python3.{i}"
        if shutil.which(python_bin_name):
            found_python_name = python_bin_name
            break

    if not found_python_name:
        logger.error("No python version found in path")
        sys.exit(1)

    return found_python_name


def _exec_python_command(command):
    try:
        process_result = subprocess.run(
            [_get_python_bin(), "-c", command],
            check=True,
            capture_output=True,
            encoding='utf-8'
        )
    except subprocess.CalledProcessError as e:
        logger.exception(f"Error during getting python site packages, output: \n{e.output}", exc_info=e)
        sys.exit(1)
    except Exception as e:
        logger.exception("Error during getting python site packages", exc_info=e)
        sys.exit(1)

    if process_result.stderr:
        logger.error(f"Error during exec python command: {process_result.stderr}")
        sys.exit(1)

    return process_result.stdout


def _get_site_packages_path() -> str:
    _check_is_python_cmd_present()

    result = _exec_python_command('import sysconfig; print(sysconfig.get_path("purelib"), end="")')

    return result


def check_layer_compatibility(layer_infos):
    _check_is_python_cmd_present()

    current_python_version = _exec_python_command(
        'import sys; '
        'print(f"python{sys.version_info.major}.{sys.version_info.minor}", end="")'
    )

    layer_compatible_versions = layer_infos["CompatibleRuntimes"]

    if current_python_version not in layer_compatible_versions:
        logger.error(f"Your python version ({current_python_version}) "
                     f"is not compatible with the layer. Compatible versions: {', '.join(layer_compatible_versions)}")
        sys.exit(1)


def overwrite_dir_or_file(path):
    if os.path.isdir(path):
        shutil.rmtree(path)
    elif os.path.isfile(path):
        os.remove(path)


class DownloadLayerComponent(AwsComponent):
    def __init__(self, app: "App", configuration):
        super().__init__(app, configuration)

        logger.info("Initializing Import value")

        self.cli_args.extend([
            {"name": "--layer-arn", "flag": "-a",
             "help": "The ARN with version of the layer to download."
                     "\nExample: \"arn:aws:lambda:${region}:${account_id}:layer:${layer_name}:${layer_version}\"",
             "required": True},
            {"name": "--download-dir", "flag": "-o",
             "help": "The directory where the layer package will be downloaded."
                     "\nDefault to the site-packages dir of the current python env.",
             "required": False, "default": None},
            {"name": "--overwrite", "flag": "-f", "action": "store_true", "default": False,
             "help": "Use this flag to allow the overwrite of files in the download directory"},
            {"name": "--disable-compatibility-check", "flag": "-d", "action": "store_true", "default": False,
             "help": "Use this flag to allow the use of a potentially incompatible layer"}
        ])

        if app.component_config_mode is ComponentConfigMode.CLI:
            self._parse_cli_args()
        else:
            logger.error("Module can only be used with CLI config mode")
            sys.exit(1)

    def _get_layer_infos(self, layer_arn) -> dict:
        session = self._boto_session()
        client = session.client('lambda')

        response = client.get_layer_version_by_arn(
            Arn=layer_arn
        )

        return response

    @staticmethod
    def _download_layer(layer_info, temp_file: tempfile.TemporaryFile):
        response = requests.get(layer_info["Content"]["Location"], stream=True)

        with Progress(
                SpinnerColumn(),
                *Progress.get_default_columns(),
                DownloadColumn(),
                TransferSpeedColumn()
        ) as progress:
            download_task = progress.add_task(description="Downloading...", total=layer_info["Content"]["CodeSize"])

            for data in response.iter_content(chunk_size=CHUNK_SIZE):
                temp_file.write(data)
                progress.update(download_task, advance=CHUNK_SIZE)

    @staticmethod
    def _setup_layer(download_dir, temp_file: tempfile.TemporaryFile, overwrite: bool):
        with tempfile.TemporaryDirectory() as temp_dir:
            with zipfile.ZipFile(temp_file) as zip_file:
                zip_file.extractall(temp_dir)
            for file in os.listdir(os.path.join(temp_dir, "python")):
                file_path = os.path.join(temp_dir, "python", file)

                if os.path.exists(os.path.join(download_dir, file)):
                    if overwrite:
                        overwrite_dir_or_file(os.path.join(download_dir, file))
                    else:
                        continue

                try:
                    shutil.move(file_path, download_dir)
                except shutil.Error:
                    logger.exception("move layer files to download dir error")

    def run(self):
        self._aws_init()

        layer_arn = self.configuration.get("layer_arn")
        if not layer_arn or not re.match(ARN_PATTERN, layer_arn):
            logger.error(f"Invalid layer arn, the arn must match regex : \"{ARN_PATTERN}\"\nARN is {layer_arn}")
            sys.exit(1)

        layer_infos = self._get_layer_infos(layer_arn)

        if not self.configuration.get("disable_compatibility_check"):
            check_layer_compatibility(layer_infos)

        default_download_dir = _get_site_packages_path()
        download_dir = self.configuration.get("download_dir", default_download_dir) or default_download_dir

        with tempfile.TemporaryFile("w+b") as temp_file:
            self._download_layer(layer_infos, temp_file)

            self._setup_layer(download_dir, temp_file, self.configuration.get("overwrite", False))

        logger.info(
            f"Successfully downloaded layer \"{layer_infos['LayerVersionArn']}\" at path \"{download_dir}\"."
            "\nIf the layer was not added to the site packages dir you will "
            "probably need to add the directory to the PYTHON_PATH."
            "\nSee: https://bic-berkeley.github.io/psych-214-fall-2016/using_pythonpath.html")
