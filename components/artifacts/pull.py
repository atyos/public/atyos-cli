#!/usr/bin/env python
import logging
import os
import sys
import zipfile
from pathlib import Path

from botocore.exceptions import UnauthorizedSSOTokenError
import semver

from app import App
from components_base import AwsComponent, ComponentConfigMode

logging.getLogger("botocore.credentials").setLevel(logging.CRITICAL)

logger = logging.getLogger("artifacts.pull")
logger.setLevel(logging.INFO)

# CONFIG OVERRIDE
ARTIFACTS_REPOSITORY = os.getenv("ARTIFACTS_REPOSITORY")  # S3 bucket where artifacts are stored


class PullComponent(AwsComponent):
    def __init__(self, app: "App", configuration):
        super().__init__(app, configuration)

        logger.info("Initializing Artifact pull")

        self.cli_args.extend([
            {"name": "--repository-bucket", "flag": "-r", "required": ARTIFACTS_REPOSITORY is None,
             "default": ARTIFACTS_REPOSITORY, "help": "Artifacts repository bucket"},
            {"name": "--artifact", "flag": "-a", "required": True,
             "help": "Name of the artifact to download (you can also use artifact:version notation)"},
            {"name": "--artifact-version", "flag": "-v", "default": "latest",
             "help": "Version of the artifact to download"},
            {"name": "--output", "flag": "-o", "help": "Path to store downloaded artifact", "required": True}
        ])

        if app.component_config_mode is ComponentConfigMode.CLI:
            self._parse_cli_args()
        else:
            logger.error("Module can only be used with CLI config mode")
            sys.exit(1)

    def get_latest(self, artifact):
        session = self._boto_session()
        bucket_name = self.configuration.get("repository_bucket")
        resource = session.resource("s3")

        version = None
        for m in resource.Bucket(bucket_name).objects.filter(Prefix=f"{artifact}/"):
            if m.key.endswith("/"):
                continue

            key_split = Path(m.key).stem.rsplit("-")
            if len(key_split) != 2:
                continue

            current_version = semver.VersionInfo.parse(key_split[1])
            if version is None or current_version > version:
                version = current_version

        return version

    def download(self, artifact: str, version: str, destination="."):
        session = self._boto_session()
        resource = session.resource("s3")

        tmp_path = f"/tmp/{artifact}-{version}.zip"

        artifact_object = resource.Object(
            self.configuration.get("repository_bucket"),
            f"{artifact}/{artifact}-{version}.zip"
        )

        artifact_object.download_file(tmp_path)

        logger.info(f"Extracting artifact archive to destination: {destination}")
        with zipfile.ZipFile(tmp_path, 'r') as zip_ref:
            zip_ref.extractall(destination)

    def run(self):
        self._aws_init()

        session = self._boto_session()
        client = session.client("s3")

        bucket_name = self.configuration.get("repository_bucket")

        logger.debug(f"Checking repository: s3://{bucket_name}")
        try:
            client.head_bucket(Bucket=self.configuration.get("repository_bucket"))
        except client.exceptions.ClientError as e:
            if e.response["Error"]["Code"] in ("404", "403"):
                logger.error(f"Cannot access S3 repository: s3://{bucket_name}")
            else:
                logger.error(e)
            sys.exit(1)
        except UnauthorizedSSOTokenError:
            logger.error(f"Invalid SSO session, run `aws sso login --profile {self.configuration.get('profile')}`")
            sys.exit(1)

        logger.debug(f"Found S3 repository: s3://{bucket_name}")

        artifact_version = self.configuration.get('artifact').rsplit(":")
        if len(artifact_version) == 2:
            artifact = artifact_version[0]
            version = artifact_version[1]
        else:
            artifact = artifact_version[0]
            version = self.configuration.get("artifact_version")

        logger.info(f"Retrieving artifact {artifact} on version {version}")

        if version == "latest":
            version = self.get_latest(artifact)
            logger.info(f"Latest version is: {artifact}-{version}.zip")
        else:
            try:
                client.head_object(Bucket=bucket_name, Key=f"{artifact}/{artifact}-{version}.zip")
            except client.exceptions.ClientError as e:
                if e.response["Error"]["Code"] in ("404", "403"):
                    logger.error(f"Unable to find artifact {artifact} in version {version}: "
                                 f"No such object s3://{bucket_name}/{artifact}/{artifact}-{version}.zip")
                else:
                    logger.error(e)
                sys.exit(1)

        logger.info(f"Downloading artifact: {artifact}:{version}")
        self.download(artifact=artifact, version=version, destination=self.configuration.get("output"))
