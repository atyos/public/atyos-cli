import os
import logging
import sys
import zipfile

from app import App
from components_base import AwsComponent, ComponentConfigMode

logger = logging.getLogger("artifacts.pull")
logger.setLevel(logging.INFO)

# CONFIG OVERRIDE
ARTIFACTS_REPOSITORY = os.getenv("ARTIFACTS_REPOSITORY")  # S3 bucket where artifacts are stored


class PushComponent(AwsComponent):
    def __init__(self, app: "App", configuration):
        super().__init__(app, configuration)

        logger.info("Initializing Artifact pull")

        self.cli_args.extend([
            {"name": "source", "help": "Source file to upload (must be a directory or file)"},
            {"name": "--repository-bucket", "flag": "-r", "required": ARTIFACTS_REPOSITORY is None,
             "default": ARTIFACTS_REPOSITORY, "help": "Artifacts repository bucket"},
            {"name": "--artifact", "flag": "-a", "required": True,
             "help": "Name of the artifact to download (you can also use artifact:version notation)"},
            {"name": "--artifact-version", "flag": "-v", "default": "latest",
             "help": "Version of the artifact to download", "required": True}
        ])

        if app.component_config_mode is ComponentConfigMode.CLI:
            self._parse_cli_args()
        else:
            logger.error("Module can only be used with CLI config mode")
            sys.exit(1)

    def package(self):
        source = self.configuration.get("source")
        artifact = self.configuration.get("artifact")
        version = self.configuration.get("artifact_version")

        if not os.path.exists(source):
            logger.error(f"Cannot find source path: {source}")
            sys.exit(1)

        tmp_file = f"/tmp/{artifact}-{version}.zip"
        archive = zipfile.ZipFile(tmp_file, "w")

        if os.path.isdir(source):
            logger.info(f"Packaging source directory: {source}")
            for dirname, sub_dirs, files in os.walk(source):
                archive.write(dirname)
                for filename in files:
                    archive.write(os.path.join(dirname, filename))
        elif os.path.isfile(source):
            logger.info(f"Packaging source file: {source}")
            archive.write(source)

        archive.close()

        return tmp_file

    def push(self, artifact_path: str):
        session = self._boto_session()
        resource = session.resource("s3")

        artifact = self.configuration.get("artifact")

        artifact_object = resource.Object(
            self.configuration.get("repository_bucket"),
            f"{artifact}/{os.path.basename(artifact_path)}"
        )

        with open(artifact_path, "rb") as fh:
            artifact_object.put(Body=fh)

    def run(self):
        self._aws_init()

        artifact_version = self.configuration.get('artifact').rsplit(":")
        if len(artifact_version) == 2:
            self.configuration["artifact"] = artifact_version[0]
            self.configuration["artifact_version"] = artifact_version[1]

        logger.info(f"Packaging artifact source: {self.configuration.get('source')}")
        tmp_file = self.package()
        logger.info(f"Successfully packaged: {tmp_file}")

        if not os.path.isfile(tmp_file):
            logger.error("An error occurred during packaging, aborting")
            sys.exit(1)

        logger.info(f"Pushing artifact to S3 repository: {self.configuration.get('repository_bucket')}")
        self.push(tmp_file)
