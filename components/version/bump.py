#!/usr/bin/env python
import logging
import os

import sys
from typing import Union, List

import semver
from git import Repo, Commit
from gitlab import Gitlab, GitlabAuthenticationError

from components_base import Component, ComponentConfigMode

logger = logging.getLogger("cicd.version_bump")
logger.setLevel(logging.getLevelName(os.getenv("LOG_LEVEL", "INFO")))

# Get branches prefixes overrides
BRANCH_PREFIX_SEPARATOR = os.environ.get("BRANCH_PREFIX_SEPARATOR", "/")
MAJOR_PREFIXES = (os.environ.get("MAJOR_PREFIXES", f"major{BRANCH_PREFIX_SEPARATOR}, release{BRANCH_PREFIX_SEPARATOR}").replace(" ", "")).split(",")
MINOR_PREFIXES = (os.environ.get("MINOR_PREFIXES", f"feature{BRANCH_PREFIX_SEPARATOR}, feat{BRANCH_PREFIX_SEPARATOR}, minor{BRANCH_PREFIX_SEPARATOR}").replace(" ", "")).split(",")
PATCH_PREFIXES = (os.environ.get("PATCH_PREFIXES", f"patch{BRANCH_PREFIX_SEPARATOR}, fix{BRANCH_PREFIX_SEPARATOR}, hotfix{BRANCH_PREFIX_SEPARATOR}").replace(" ", "")).split(",")

# Get MR labels overrides
MAJOR_LABELS = (os.environ.get("MAJOR_LABELS", "major, release").replace(" ", "")).split(",")
MINOR_LABELS = (os.environ.get("MINOR_LABELS", "feature, feat, minor").replace(" ", "")).split(",")
PATCH_LABELS = (os.environ.get("PATCH_LABELS", "patch, fix, hotfix").replace(" ", "")).split(",")

# Get commit labels overrides
MAJOR_COMMIT_LABELS = (os.environ.get("MAJOR_COMMIT_LABELS", "major, release").replace(" ", "")).split(",")
MINOR_COMMIT_LABELS = (os.environ.get("MINOR_COMMIT_LABELS", "feature, feat, minor").replace(" ", "")).split(",")
PATCH_COMMIT_LABELS = (os.environ.get("PATCH_COMMIT_LABELS", "*").replace(" ", "")).split(",")

# CI_VARIABLES
GITLAB_TOKEN = os.environ.get("GITLAB_TOKEN")
GITLAB_CI = os.environ.get("GITLAB_CI")
CI_COMMIT_REF_PROTECTED = os.environ.get("CI_COMMIT_REF_PROTECTED")
CI_SERVER_URL = os.environ.get("CI_SERVER_URL")
CI_PROJECT_ID = os.environ.get("CI_PROJECT_ID")
CI_SERVER_HOST = os.environ.get("CI_SERVER_HOST")
CI_PROJECT_PATH = os.environ.get("CI_PROJECT_PATH")


# By Label
def revision_from_mr_label(labels: List[str]):
    revision_type: str = ""
    if any([patch_label in labels for patch_label in PATCH_LABELS]):
        revision_type += "patch"
    if any([minor_label in labels for minor_label in MINOR_LABELS]):
        revision_type += "minor"
    if any([major_label in labels for major_label in MAJOR_LABELS]):
        revision_type += "major"

    if revision_type == "":
        logger.error("Unable to find revision type label. Cannot resolve revision type. Exiting")
        sys.exit(1)
    elif revision_type not in ["major", "minor", "patch"]:
        logger.error("Ambiguous situation... multiple revision labels found on merge request. Exiting")
        sys.exit(1)

    return revision_type


# By branch name
def revision_from_branch(branch_name: str):
    if any([branch_name.startswith(patch_prefix) for patch_prefix in PATCH_PREFIXES]):
        return "patch"
    elif any([branch_name.startswith(minor_prefix) for minor_prefix in MINOR_PREFIXES]):
        return "minor"
    elif any([branch_name.startswith(major_prefix) for major_prefix in MAJOR_PREFIXES]):
        return "major"
    else:
        logger.error("Branch name doesn't follow branching convention. Cannot resolve revision type. Exiting")
        sys.exit(1)


# From commits messages
def revision_from_last_commit(commit: Commit, repository: Repo):
    revision_type: str = ""
    for c in repository.iter_commits():
        if c == commit or revision_type == "major":
            break
        commit_message = c.message.replace("\n", " ")
        if any([commit_message.startswith(label) for label in MAJOR_COMMIT_LABELS]):
            revision_type = "major"
        elif any([commit_message.startswith(label) for label in MINOR_COMMIT_LABELS]):
            revision_type = "minor"
        elif any([commit_message.startswith(label) for label in PATCH_COMMIT_LABELS]) and revision_type != "minor":
            revision_type = "patch"

        logger.info(f"Commit message: \"{commit_message}\" [Revision type: {revision_type or 'Undefined'}]")

    # Fallback to default revision type if any (marked with *)
    if not revision_type and "*" in PATCH_COMMIT_LABELS:
        revision_type = "patch"
    if not revision_type and "*" in MINOR_COMMIT_LABELS:
        revision_type = "minor"
    if not revision_type and "*" in MAJOR_COMMIT_LABELS:
        revision_type = "major"

    return revision_type


class BumpComponent(Component):
    def __init__(self, app: "App", configuration):
        super().__init__(app, configuration)

        logger.info("Initializing Version Bump")

        self.cli_args.extend([
            {"name": "--repository-path", "help": "Path to the repository to version"},
            {"name": "--output-file", "help": "Output version number to specified file"}
        ])

        if app.component_config_mode is ComponentConfigMode.CLI:
            self._parse_cli_args()
        else:
            logger.error("Module can only be used with CLI config mode")
            sys.exit(1)

    def create_version_artifact(self, version: Union[semver.VersionInfo, str]):
        output_file = self.configuration.get("output_file")
        if not output_file:
            return

        logger.info(f"Writing version to output file: {output_file}")
        with open(output_file, "w+") as fh:
            fh.write(str(version))

    def run(self):
        if not GITLAB_CI:
            logger.error("This script is not intended to be run locally. Exiting")
            sys.exit(1)

        if not CI_COMMIT_REF_PROTECTED:
            logger.error("Build branch must be protected. Exiting.")
            sys.exit(1)

        if not GITLAB_TOKEN:
            logger.error("Missing GITLAB_TOKEN CI/CD variable.")
            logger.error("Please provide the project with a GITLAB_TOKEN variable (permissions: read_api).")
            sys.exit(1)

        # Resolve default repository path
        if not self.configuration.get("repository_path"):
            if GITLAB_CI:
                self.configuration["repository_path"] = os.getenv("CI_PROJECT_DIR")
            else:
                logger.error("Missing --repository-path argument")
                sys.exit(1)

        # Configure Git
        repository = Repo(self.configuration.get("repository_path"), search_parent_directories=True)
        remote_url = f"https://oauth2:{GITLAB_TOKEN}@{CI_SERVER_HOST}/{CI_PROJECT_PATH}.git"
        logger.info(f"Configured remote_url: {remote_url}")
        repository.remotes.origin.set_url(remote_url)
        with repository.config_writer() as wh:
            wh.set_value("user", "name", "Gitlab Runner")
            wh.set_value("user", "email", f"gitlab-runner@{CI_SERVER_HOST}")

        current_commit = repository.head.commit
        logger.info(f"Current commit: {current_commit.message} [{current_commit.hexsha}]")

        try:
            gitlab = Gitlab(CI_SERVER_URL, private_token=GITLAB_TOKEN)
            gitlab.auth()
            gitlab_project = gitlab.projects.get(CI_PROJECT_ID)
            merge_request = next(filter(
                lambda mr: mr.merge_commit_sha == current_commit.hexsha,
                gitlab_project.mergerequests.list()
            ), None)
        except GitlabAuthenticationError as e:
            logger.error("Invalid token. Please provide a valid token and retry. Exiting.")
            sys.exit(1)

        # Parse versions tags
        versions = {semver.VersionInfo.parse("0.0.0"): None}
        logger.debug("Finding last version tag")
        for tag in repository.tags:
            try:
                version_info = semver.VersionInfo.parse(tag.name)
                logger.debug(f"Found version tag: {tag.name} [{tag.commit}]")
                versions[version_info] = tag.commit
                if tag.commit == current_commit:
                    logger.info(f"Current commit already tagged with version {version_info}. Nothing to do.")
                    self.create_version_artifact(version_info)
                    sys.exit(0)
            except ValueError:
                logger.debug(f"Tag \"{tag.name}\" is not a semver tag. Skipping")
                continue

        # Resolve revision type
        current_version = max(versions.keys())
        if merge_request and os.environ.get("GITLAB_MR_LABEL_MODE"):
            logger.info("Resolving revision type from Merge Request labels")
            logger.info(f"Merge Request Labels: {merge_request.labels}")
            revision_type = revision_from_mr_label(merge_request.labels)
        elif merge_request:
            logger.info("Resolving revision type from merge source branch name")
            logger.info(f"Source branch: {merge_request.source_branch}")
            revision_type = revision_from_branch(merge_request.source_branch)
        else:
            # Get all commits since last tag
            logger.info("Resolving revision type from commits messages")
            last_commit = versions[current_version] or repository.head.commit
            logger.info(f"Last versioned commit {last_commit.hexsha} [{current_version}]")
            revision_type = revision_from_last_commit(last_commit, repository)

        # Bump version
        if revision_type == "major":
            new_version = current_version.bump_major()
        elif revision_type == "minor":
            new_version = current_version.bump_minor()
        elif revision_type == "patch":
            new_version = current_version.bump_patch()
        else:
            logger.error("Unable to parse revision type. You break the CI/CD. Exiting.")
            sys.exit(1)

        logger.info(f"Current version: {current_version}")
        logger.info(f"New version: {new_version} [Revision_type: {revision_type}]")
        logger.info(f"Pushing tag '{new_version}' to repository")

        # Push new version tag
        gitlab_project.tags.create({
            "tag_name": str(new_version),
            "ref": str(current_commit.hexsha),
            "message": f"Version {new_version} (tagged by CI/CD Runner)"
        })

        self.create_version_artifact(str(new_version))
