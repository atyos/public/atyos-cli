import logging
import os
import sys

from components_base import AwsComponent, ComponentConfigMode
from library import io
from library.mgn_drs import service_methods as mgn

logger = logging.getLogger("component.base")
logger.setLevel(logging.getLevelName(os.getenv("LOG_LEVEL", "INFO")))


class PullComponent(AwsComponent):

    def __init__(self, app: "App", configuration):
        super(PullComponent, self).__init__(app, configuration)

        logger.info("Initializing DRS Source servers template pull")

        self.cli_args.extend([
            {"name": "--output-path", "flag": "-o", "help": "Path to output root", "required": True},
            {"name": "--show-default", "flag": "-d", "help": "Display default source server with all fields available",
             "default": False, "required": False},
        ])

        if app.component_config_mode is ComponentConfigMode.CLI:
            self._parse_cli_args()
        else:
            logger.error("Module can only be used with CLI config mode")
            sys.exit(1)

    def run(self):
        self._aws_init()
        output_path = self.configuration.get("output_path")
        session = self._boto_session()
        mgn_client = session.client('mgn')
        ec2_client = session.client('ec2')
        logger.info("Start pulling current DRS source servers templates")
        mgn_configuration = mgn.get_current_configuration(mgn_client, ec2_client)
        mgn_configuration_as_dict = mgn_configuration.export(self.configuration.get("show_default", False))
        io.export_dict_as_yaml(mgn_configuration_as_dict, output_path)
