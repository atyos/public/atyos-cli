import logging
import os
import sys

from components_base import AwsComponent, ComponentConfigMode
from library import io
from library.mgn_drs import service_methods as mgn
from library.mgn_drs.diff_methods import export_diff, display_diff

logger = logging.getLogger("component.base")
logger.setLevel(logging.getLevelName(os.getenv("LOG_LEVEL", "INFO")))


class DiffComponent(AwsComponent):

    def __init__(self, app: "App", configuration):
        super(DiffComponent, self).__init__(app, configuration)

        logger.info("Initializing DRS Source servers template diff")

        self.cli_args.extend([
            {"name": "--template-path", "flag": "-t", "help": "Path to template", "required": True},
            {"name": "--output-path", "flag": "-o", "help": "Path to diff file", "required": False},
        ])

        if app.component_config_mode is ComponentConfigMode.CLI:
            self._parse_cli_args()
        else:
            logger.error("Module can only be used with CLI config mode")
            sys.exit(1)

    def run(self):
        self._aws_init()
        template_path = self.configuration.get("template_path")
        output_path = self.configuration.get("output_path")

        session = self._boto_session()
        mgn_client = session.client('mgn')
        ec2_client = session.client('ec2')

        input_state = io.open_yaml(template_path)

        parameters = input_state.get("parameters", {})
        source_servers = input_state.get("launchTemplates", {})

        new_configuration = mgn.get_new_configuration(parameters, source_servers)
        current_configuration = mgn.get_current_configuration(mgn_client, ec2_client)
        diff = {"current_template": {}, "new_template": {}}
        for server_name, server_configuration in new_configuration.LaunchTemplates.items():
            current_server_configuration = current_configuration.LaunchTemplates.get(server_name)
            local_diff = current_server_configuration.compare_with(server_configuration)
            if local_diff:
                diff["current_template"][server_name] = local_diff.get("current_template")
                diff["new_template"][server_name] = local_diff.get("new_template")

        if output_path:
            export_diff(diff, output_path)

        display_diff(diff)
