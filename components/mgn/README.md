# ATYOS CLI MGN COMPONENT  
  
This component allow to check MGN status and to get/update source servers from MGN   
  
## mgn/diff  
Get the difference between actual state of MGN and source servers from template  
  
> `--template-path`  
  Path to template  
  Required : True  
  Flag : -t  
  
> `--output-path`  
  Path to output file for difference export  
  Required : false  
  Flag : -o  
  
### Example  
```
atyos mgn/diff -t TEMPLATE_FILE_PATH --profile PROFILE_NAME
```
  
## mgn/pull  
Get the actual state of MGN source servers.  
  
[Click here](./output.yaml) for an output example 
  
> `--output-path`  
  Path to output file  
  Required : True  
  Flag : -o  
  
> `--show-default`  
  Display default source server with all fields available  
  Required : False  
  Flag : -d  
  
### Example  
```
atyos mgn/pull -o OUTPUT_PATH -d True --profile PROFILE_NAME
```
  
## mgn/status  
Get the status of MGN  
  
### Example  
```
atyos mgn/status --profile PROFILE_NAME
```  
  
## mgn/update  
Update MGN with source servers from template  
  
> `--template-path`  
  Path to template  
  Required : True  
  Flag : -t  
  
> `--output-path`  
  Path to output file for difference export  
  Required : false  
  Flag : -o  
  
> `--display-diff`  
  Display difference  
  Required : false  
  Flag : -d  
  
> `--force` 
  Automatically skip server with validation error  
  Required : false  
  Flag : -f  
  
> `--dry-run`  
  Dry Run  
  Required : false  
  Flag : -n  

### Example  
```
atyos mgn/update -t TEMPLATE_FILE_PATH --profile PROFILE_NAME
```
  
