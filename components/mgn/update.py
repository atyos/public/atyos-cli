import logging
import os
import sys

from components_base import AwsComponent, ComponentConfigMode
from library import io
from library.aws_ressources.ec2 import create_launch_template, update_default_launch_template, create_tags
from library.mgn_drs import service_methods as mgn
from library.mgn_drs.diff_methods import export_diff, display_diff

logger = logging.getLogger("component.base")
logger.setLevel(logging.getLevelName(os.getenv("LOG_LEVEL", "INFO")))


class UpdateComponent(AwsComponent):

    def __init__(self, app: "App", configuration):
        super(UpdateComponent, self).__init__(app, configuration)

        logger.info("Initializing DRS Source servers template Update")

        self.cli_args.extend([
            {"name": "--template-path", "flag": "-t", "help": "Path to template", "required": True},
            {"name": "--output-path", "flag": "-o", "help": "Path to diff file", "required": False},
            {"name": "--display-diff", "flag": "-d", "help": "Display diff", "required": False},
            {"name": "--dry-run", "flag": "-n", "help": "Dry run", "required": False, "default": False},
        ])

        if app.component_config_mode is ComponentConfigMode.CLI:
            self._parse_cli_args()
        else:
            logger.error("Module can only be used with CLI config mode")
            sys.exit(1)

    def run(self):
        self._aws_init()
        template_path = self.configuration.get("template_path")
        output_path = self.configuration.get("output_path")
        do_display_diff = self.configuration.get("display_diff")
        dry_run = self.configuration.get("dry_run")

        session = self._boto_session()
        mgn_client = session.client('mgn')
        ec2_client = session.client('ec2')

        input_state = io.open_yaml(template_path)

        parameters = input_state.get("parameters", {})
        source_servers = input_state.get("launchTemplates", {})

        new_configuration = mgn.get_new_configuration(parameters, source_servers)
        current_configuration = mgn.get_current_configuration(mgn_client, ec2_client)
        diff = {"current_template": {}, "new_template": {}}
        for server_name, server_configuration in new_configuration.LaunchTemplates.items():
            current_server_configuration = current_configuration.LaunchTemplates.get(server_name)
            if output_path or do_display_diff:
                local_diff = current_server_configuration.compare_with(server_configuration)
                if local_diff:
                    diff["current_template"][server_name] = local_diff.get("current_template")
                    diff["new_template"][server_name] = local_diff.get("new_template")

            current_server_configuration.update_fields(server_configuration)
        if output_path or do_display_diff:
            if output_path:
                export_diff(diff, output_path)
            if do_display_diff:
                display_diff(diff)
        templates_names = current_configuration.Mappings.TemplatesName
        current_configuration.Mappings.update_mapping(new_configuration.Mappings.Subnets,
                                                      new_configuration.Mappings.SecurityGroups)
        current_configuration.pre_save()
        for server_name, server_configuration in current_configuration.LaunchTemplates.items():
            if server_configuration.Ignore:
                logger.info(f"Template {server_name} ignored")
                continue
            template_id = templates_names.get(server_name)
            if not template_id:
                logger.info(f"Template {server_name} can't be update. Mapping with LaunchTemplateID impossible")
                continue

            if dry_run:
                logger.info(f"Dry run : Template {server_name} would be updated")
                continue
            instance = create_launch_template(ec2_client, template_id, server_name,
                                              server_configuration.dict(exclude_none=True))

            if instance:
                logger.info(f"Template {server_name} updated")
                version = instance.get('LaunchTemplateVersion', {}).get('VersionNumber')
                if version:
                    update_default_launch_template(ec2_client, template_id, str(version))
                    create_tags(ec2_client, template_id, [{'Key': 'Name', 'Value': server_name}])
                    logger.info(f"Template {server_name} version updated to {version}")
            else:
                logger.error(f"Template {server_name} not updated")
