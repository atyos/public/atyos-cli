#!/usr/bin/env python
import logging
import os

import sys

from components_base import Component, ComponentConfigMode

logger = logging.getLogger("terraform.apply")
logger.setLevel(logging.getLevelName(os.getenv("LOG_LEVEL", "INFO")))


class ApplyComponent(Component):
    def __init__(self, app: "App", configuration):
        super().__init__(app, configuration)

        logger.info("Initializing Terraform Apply")

        if app.component_config_mode is ComponentConfigMode.CLI:
            self._parse_cli_args()
        else:
            logger.error("Module can only be used with CLI config mode")
            sys.exit(1)

    def run(self):
        terraform_command = "terraform apply -auto-approve"

        logger.info(f"Applying terraform infrastructure in the Cloud")

        if self.configuration.get("dry_run"):
            logger.info(terraform_command)
        else:
            return_code = os.system(terraform_command)
            if return_code != 0:
                logger.error(f"Terraform apply command exited with non-zero exit code [{return_code}]")
                logger.error(f"Command: {terraform_command}")
                sys.exit(1)
