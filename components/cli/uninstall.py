#!/usr/bin/env python
import logging
import os
import shutil
import sys

from app import App
from components_base import Component, ComponentConfigMode

logger = logging.getLogger("cli.uninstall")
logger.setLevel(logging.INFO)


def is_elf(path: str):
    with open(path, "rb") as f:
        first_four = f.read(4)

    return first_four == b"\x7fELF"


class UninstallComponent(Component):
    def __init__(self, app: "App", configuration):
        super().__init__(app, configuration)

        logger.info("Initializing Atyos CLI installation")

        self.cli_args.extend([
            {"name": "--destination", "flag": "-d", "default": "/usr/bin", "required": False,
             "help": "Path to install folder. (Default is /usr/bin)"}
        ])

        if app.component_config_mode is ComponentConfigMode.CLI:
            self._parse_cli_args()
        else:
            logger.error("Module can only be used with CLI config mode")
            sys.exit(1)

    def run(self):
        destination = os.path.realpath(f"{self.configuration.get('destination')}/atyos")

        if not is_elf(destination):
            logger.error("Cannot uninstall source files")
            sys.exit(1)

        logger.info(f"Uninstalling atyos-cli from {self.configuration.get('destination')}")
        try:
            os.remove(destination)
        except PermissionError:
            logger.error(f"Cannot uninstall atyos-cli to {destination}. Try using sudo or use --destination to change"
                         f" install directory.")
            sys.exit(1)
        except FileNotFoundError:
            logger.error(f"Cannot find atyos-cli installation into {destination}.")
            sys.exit(1)

        logger.info("See you soon :) .")
