#!/usr/bin/env python
import logging
import os
import shutil
import sys

from app import App
from components_base import Component, ComponentConfigMode

logger = logging.getLogger("cli.install")
logger.setLevel(logging.INFO)


def is_elf(path: str):
    with open(path, "rb") as f:
        first_four = f.read(4)

    return first_four == b"\x7fELF"


class InstallComponent(Component):
    def __init__(self, app: "App", configuration):
        super().__init__(app, configuration)

        logger.info("Initializing Atyos CLI installation")

        self.cli_args.extend([
            {"name": "--destination", "flag": "-d", "default": "/usr/bin", "required": False,
             "help": "Path to install folder. (Default is /usr/bin)"}
        ])

        if app.component_config_mode is ComponentConfigMode.CLI:
            self._parse_cli_args()
        else:
            logger.error("Module can only be used with CLI config mode")
            sys.exit(1)

    def run(self):
        binary_path = os.path.abspath(sys.argv[0])
        destination = os.path.realpath(f"{self.configuration.get('destination')}/atyos")

        if not is_elf(binary_path):
            logger.error("Cannot install from source")
            sys.exit(1)

        logger.info(f"Installing atyos-cli to {self.configuration.get('destination')}")
        try:
            shutil.copy(binary_path, destination)
        except PermissionError:
            logger.error(f"Cannot install atyos-cli to {destination}. Try using sudo or use --destination to change"
                         f" install directory.")
            sys.exit(1)

        logger.info("Done :). You can now run 'atyos' command.")
