import logging
import os
import sys

from rich.console import Console
from rich.table import Table

from components.drs.ressources.drs_methods import get_source_server
from components_base import AwsComponent, ComponentConfigMode

logger = logging.getLogger("component.base")
logger.setLevel(logging.getLevelName(os.getenv("LOG_LEVEL", "INFO")))


class StatusComponent(AwsComponent):

    def __init__(self, app: "App", configuration):
        super(StatusComponent, self).__init__(app, configuration)

        logger.info("Initializing DRS Source servers template diff")

        if app.component_config_mode is ComponentConfigMode.CLI:
            self._parse_cli_args()
        else:
            logger.error("Module can only be used with CLI config mode")
            sys.exit(1)

    def run(self):
        self._aws_init()
        session = self._boto_session()
        drs_client = session.client('drs')
        source_servers = get_source_server(drs_client)
        table = Table(title="DRS Source servers Status")

        table.add_column("Name", justify="center")
        table.add_column("Status", justify="center")
        table.add_column("Lag", justify="center")
        table.add_column("Error", justify="center")
        for source_server in source_servers:
            name = source_server.get("tags").get("Name")
            status = source_server.get('dataReplicationInfo', {}).get('dataReplicationState')
            if status == 'CONTINUOUS':
                status_value = f"[green]{status}[/green]"
            else:
                status_value = f"[red]{status}"
            error = None
            if 'dataReplicationError' in source_server.get('dataReplicationInfo', {}):
                error = source_server.get('dataReplicationInfo', {}).get('dataReplicationError', {}).get("error")
            table.add_row(name, status_value, source_server.get('dataReplicationInfo', {}).get('lagDuration'), error)

        console = Console()
        console.print(table)
