import logging
import os
import sys

from components.drs.ressources import drs_methods as drs, io
from components.drs.ressources.diff_methods import export_diff, display_diff, get_diff_dict
from components_base import AwsComponent, ComponentConfigMode

logger = logging.getLogger("component.base")
logger.setLevel(logging.getLevelName(os.getenv("LOG_LEVEL", "INFO")))


class DiffComponent(AwsComponent):

    def __init__(self, app: "App", configuration):
        super(DiffComponent, self).__init__(app, configuration)

        logger.info("Initializing DRS Source servers template diff")

        self.cli_args.extend([
            {"name": "--template-path", "flag": "-t", "help": "Path to template", "required": True},
            {"name": "--output-path", "flag": "-o", "help": "Path to diff file", "required": False},
        ])

        if app.component_config_mode is ComponentConfigMode.CLI:
            self._parse_cli_args()
        else:
            logger.error("Module can only be used with CLI config mode")
            sys.exit(1)

    def run(self):
        self._aws_init()
        template_path = self.configuration.get("template_path")
        output_path = self.configuration.get("output_path")

        session = self._boto_session()
        drs_client = session.client('drs')
        ec2_client = session.client('ec2')

        input_state = io.open_yaml(template_path)

        core_servers = input_state.get("launchTemplates", {})

        current_state = drs.get_current_state(drs_client, ec2_client)
        current_core_servers = current_state.export_as_dict().get("launchTemplates", {})

        diff = get_diff_dict(current_core_servers, core_servers)

        if output_path:
            export_diff(diff, output_path)

        display_diff(diff)
