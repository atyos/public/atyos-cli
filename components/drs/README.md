# ATYOS CLI DRS COMPONENT  
  
This component allow to check DRS status and to get/update source servers from DRS   
  
## drs/diff  
Get the difference between actual state of DRS and source servers from template  
  
> `--template-path`  
  Path to template  
  Required : True  
  Flag : -t  
  
> `--output-path`  
  Path to output file for difference export  
  Required : false  
  Flag : -o  
  
### Example  
```
atyos drs/diff -t TEMPLATE_FILE_PATH --profile PROFILE_NAME
```
  
## drs/pull  
Get the actual state of DRS source servers.  
  
[Click here](./output.yaml) for an output example 
  
> `--output-path`  
  Path to output file  
  Required : True  
  Flag : -o  
  
> `--show-default`  
  Display default source server with all fields available  
  Required : False  
  Flag : -d  
  
### Example  
```
atyos drs/pull -o OUTPUT_PATH -d True --profile PROFILE_NAME
```
  
## drs/status  
Get the status of DRS  
  
### Example  
```
atyos drs/status --profile PROFILE_NAME
```  
  
## drs/update  
Update DRS with source servers from template  
  
> `--template-path`  
  Path to template  
  Required : True  
  Flag : -t  
  
> `--output-path`  
  Path to output file for difference export  
  Required : false  
  Flag : -o  
  
> `--display-diff`  
  Display difference  
  Required : false  
  Flag : -d  
  
> `--force` 
  Automatically skip server with validation error  
  Required : false  
  Flag : -f  
  
> `--dry-run`  
  Dry Run  
  Required : false  
  Flag : -n  

### Example  
```
atyos drs/update -t TEMPLATE_FILE_PATH --profile PROFILE_NAME
```
  
