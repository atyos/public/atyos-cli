import logging
import os
import sys

from components.drs.ressources import drs_methods as drs, io
from components.drs.ressources.ClientState import ClientState
from components.drs.ressources.diff_methods import get_diff_dict, export_diff, display_diff
from components.drs.ressources.ec2 import create_launch_template, update_default_launch_template, create_tags
from components_base import AwsComponent, ComponentConfigMode

logger = logging.getLogger("component.base")
logger.setLevel(logging.getLevelName(os.getenv("LOG_LEVEL", "INFO")))


class UpdateComponent(AwsComponent):

    def __init__(self, app: "App", configuration):
        super(UpdateComponent, self).__init__(app, configuration)

        logger.info("Initializing DRS Source servers template Update")

        self.cli_args.extend([
            {"name": "--template-path", "flag": "-t", "help": "Path to template", "required": True},
            {"name": "--output-path", "flag": "-o", "help": "Path to diff file", "required": False},
            {"name": "--display-diff", "flag": "-d", "help": "Display diff", "required": False},
            {"name": "--force", "flag": "-f", "help": "Automatically skip server with validation error",
             "required": False, "default": False},
            {"name": "--dry-run", "flag": "-n", "help": "Dry run", "required": False, "default": False},
        ])

        if app.component_config_mode is ComponentConfigMode.CLI:
            self._parse_cli_args()
        else:
            logger.error("Module can only be used with CLI config mode")
            sys.exit(1)

    def run(self):
        self._aws_init()
        template_path = self.configuration.get("template_path")
        output_path = self.configuration.get("output_path")
        do_display_diff = self.configuration.get("display_diff")
        dry_run = self.configuration.get("dry_run")
        force = self.configuration.get("force")

        session = self._boto_session()
        drs_client = session.client('drs')
        ec2_client = session.client('ec2')

        input_state = io.open_yaml(template_path)

        parameters = input_state.get("parameters", {})
        core_servers = input_state.get("launchTemplates", {})
        client_state = ClientState.import_from_yaml(parameters=parameters, core_servers=core_servers, force=force)

        current_state = drs.get_current_state(drs_client, ec2_client)
        if output_path or do_display_diff:
            diff = get_diff_dict(current_state.export_as_dict().get("launchTemplates", {}), core_servers)

            if output_path:
                export_diff(diff, output_path)
            if do_display_diff:
                display_diff(diff)

        additional_data = current_state.additional_data

        for template_name, current_state_template in current_state.launch_templates.items():
            template_id = additional_data[template_name].get("launchTemplateId")

            client_state_template = client_state.launch_templates.get(template_name)

            if not client_state_template:
                logger.info(f"Template {template_name} not found in client state or ignored, skipping")
                continue

            launch_template = ClientState.get_complete_launch_template(current_state_template,
                                                                       client_state_template,
                                                                       additional_data[template_name])
            if dry_run:
                logger.info(f"Dry run : Template {template_name} would be updated")
                continue
            instance = create_launch_template(ec2_client, template_id, template_name, launch_template)

            if instance:
                logger.info(f"Template {template_name} updated")
                version = instance.get('LaunchTemplateVersion', {}).get('VersionNumber')
                if version:
                    update_default_launch_template(ec2_client, template_id, str(version))
                    create_tags(ec2_client, template_id, [{'Key': 'Name', 'Value': template_name}])
                    logger.info(f"Template {template_name} version updated to {version}")
            else:
                logger.error(f"Template {template_name} not updated")
