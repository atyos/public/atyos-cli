import logging
import os
import sys
from typing import Dict

import yaml
from rich.prompt import Prompt

logger = logging.getLogger("component.base")
logger.setLevel(logging.getLevelName(os.getenv("LOG_LEVEL", "INFO")))


def open_yaml(input_name) -> Dict:
    if not os.path.isfile(input_name) or not input_name.endswith(("yml", "yaml")):
        logger.error("--template-path must be a yaml file")
        sys.exit(1)
    # Open the YAML file
    with open(input_name, 'r') as f:
        # Load the YAML data into a Python object
        data = yaml.safe_load(f)
    return data


def export_dict_as_yaml(state_as_dict, output_name):
    with open(output_name, 'w') as f:
        yaml.safe_dump(state_as_dict, f, sort_keys=False)


def continue_with_error():
    return_value = Prompt.ask("Continue with error?", choices=["y", "N"], default="N", show_default=False)
    if return_value == "y":
        return True
    return False
