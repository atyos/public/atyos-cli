import json

from rich.console import Console
from rich.table import Table

from components.drs.ressources import io


def add_color(element, color):
    if isinstance(element, dict):
        return {k: add_color(v, color) for k, v in element.items()}
    elif isinstance(element, list):
        return [add_color(e, color) for e in element]
    else:
        return f"[{color}]{element}[/{color}]"


def add_colors(current_template, new_template):
    if isinstance(current_template, dict):
        for key, value in current_template.items():
            current_template[key], new_template[key] = add_colors(current_template[key],
                                                                  new_template[key])
    elif isinstance(current_template, list):
        current_template = add_color(current_template, "red")
        new_template = add_color(new_template, "green")
    else:
        if new_template is None:
            current_template = add_color(current_template, "red")
        else:
            new_template = add_color(new_template, "green")

    return current_template, new_template


def display_diff(diff):
    current_template = diff.get("current_template")
    new_template = diff.get("new_template")

    current_template, new_template = add_colors(current_template, new_template)
    table = Table(title="DRS Launch Template Diff", show_lines=True)

    table.add_column("Launch template name", justify="center", vertical="middle")
    table.add_column("Current template")
    table.add_column("New template")
    for key, value in current_template.items():
        table.add_row(key, json.dumps(value, indent=2), json.dumps(new_template[key], indent=2))

    console = Console()
    console.print(table)


def create_export(current_template, new_template):
    export_template = {}
    if isinstance(current_template, dict):
        for key, value in current_template.items():
            export_template[key] = create_export(current_template[key],
                                                 new_template[key])
    else:
        export_template = {"current_value": current_template, "new_value": new_template}

    return export_template


def export_diff(diff, output_path):
    current_template = diff.get("current_template")
    new_template = diff.get("new_template")

    export_template = create_export(current_template, new_template)

    io.export_dict_as_yaml(export_template, output_path)


def get_diff_dict(current_template, new_template):
    diff_dict = {"current_template": {}, "new_template": {}}
    for key, value in current_template.items():
        if key in new_template:
            if isinstance(value, dict):
                return_value = get_diff_dict(value, new_template[key])
                if return_value["current_template"] != {}:
                    diff_dict["current_template"][key] = return_value["current_template"]
                    diff_dict["new_template"][key] = return_value["new_template"]
            else:
                if value != new_template[key]:
                    diff_dict["current_template"][key] = value
                    diff_dict["new_template"][key] = new_template[key]
        # if value is not in new_template we keep the value in current_template

    for key, value in new_template.items():
        if key not in current_template:
            diff_dict["current_template"][key] = None
            diff_dict["new_template"][key] = value

    return diff_dict
