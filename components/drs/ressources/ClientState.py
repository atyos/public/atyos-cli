import base64
import ipaddress
import json
import logging
import os
from enum import Enum

from rich.console import Console
from rich.table import Table

import components.drs.ressources.drs_methods as drs
from components.drs.ressources.NetworkInterface import NetworkInterface
from components.drs.ressources.io import continue_with_error

logger = logging.getLogger("component.base")
logger.setLevel(logging.getLevelName(os.getenv("LOG_LEVEL", "INFO")))


class PrivateIPType(Enum):
    CREATE_NEW = "CREATE NEW"
    COPY_SOURCE = "COPY SOURCE"


class ClientState:
    default_parameter = {
        "disks": {
            "VolumeType": "gp3",
            "Throughput": 150,
            "Iops": 3000
        }
    }
    _default_template = {
        "ignore": {
            "help": "Ignore this template",
            "type": bool
        },
        "ip": {
            "help": "The primary private IPv4 address of the network interface",
            "type": str,
            "choices": ["CREATE NEW", "COPY SOURCE", "IP"]
        },
        "subnet": {
            "help": "The subnet for the network interface",
            "type": str
        },
        "securityGroups": {
            "help": "One or more security groups.",
            "type": list,
            "elements_type": str
        },
        "instanceType": {
            "help": "Instance type",
            "type": str
        },
        "imageId": {
            "help": "The ID of the AMI",
            "type": str
        },
        "publicIPAction": {
            "help": "Associates a public IPv4 address with eth0 for a new network interface",
            "type": bool,
        },
        "deleteOnTerminationInterface": {
            "help": "Indicates whether the network interface is deleted when the instance is terminated.",
            "type": bool
        },
        "networkInterfaceId": {
            "help": "The ID of the network interface",
            "type": str
        },
        "iamInstanceRole": {
            "help": "IAM role name of the instance profile",
            "type": str
        },
        "disks": {
            "element":
                {
                    "deviceName": {
                        "element": {
                            "type": {
                                "help": "Volume type of the disk",
                                "type": str
                            },
                            "throughput": {
                                "help": "The throughput to provision for a gp3 volume",
                                "type": int
                            },
                            "iops": {
                                "help": "The number of I/O operations per second",
                                "type": int
                            },
                            "size": {
                                "help": "The size of the volume, in GiBs",
                                "type": int
                            },
                            "kms_key_id": {
                                "help": "The ARN of the symmetric Key Management Service (KMS) CMK used for encryption",
                                "type": str
                            },
                            "snapshotId": {
                                "help": "The ID of the snapshot",
                                "type": str
                            }
                        }
                    },

                }
        },
        "userData": {
            "help": "The user data to make available to the instance",
            "type": str
        },
        "keyName": {
            "help": "The name of the key pair",
            "type": str

        },
        "enableMonitoring": {
            "help": "Specify true to enable detailed monitoring. Otherwise, basic monitoring is enabled",
            "type": bool
        },
        "disableApiTermination": {
            "help": "If you set this parameter to true , you can't terminate the instance using the Amazon EC2 console, CLI, or API; otherwise, you can",
            "type": bool
        },
        "disableApiStop": {
            "help": "Indicates whether to enable the instance for stop protection",
            "type": bool
        },
        "instanceInitiatedShutdownBehavior": {
            "help": "Indicates whether an instance stops or terminates when you initiate shutdown from the instance",
            "type": str,
            "choices": ["stop", "terminate"]
        },
        "creditSpecification": {
            "help": "The credit option for CPU usage of a T instance",
            "type": str,
            "choices": ["standard", "unlimited"]
        },
        "tags": {
            "element": {
                "Key": {
                    "help": "Key of the tag",
                    "type": str
                },
                "Value": {
                    "help": "Value of the tag",
                    "type": str
                }
            }
        }

    }

    def __init__(self,
                 launch_templates: dict,
                 additional_data: dict = None,
                 network_interface: NetworkInterface = None,
                 parameter_subnets: dict = None,
                 parameter_security_groups: dict = None,
                 parameter_tags: dict = None):
        self.launch_templates = launch_templates
        if additional_data:
            self.additional_data = additional_data
        if network_interface:
            self.mapping_subnets = network_interface.mapping_subnet
            self.mapping_security_groups = network_interface.mapping_security_groups
            self.parameter_subnets = network_interface.parameter_subnets
            self.parameter_security_groups = network_interface.parameter_security_groups
        if parameter_subnets:
            self.parameter_subnets = parameter_subnets
        if parameter_security_groups:
            self.parameter_security_groups = parameter_security_groups
        if parameter_tags:
            self.parameter_tags = parameter_tags
        else:
            self.parameter_tags = {}

    @staticmethod
    def import_from_yaml(parameters: dict, core_servers: dict, force=False):
        parameter_subnets = parameters.get("subnets", {})
        parameter_security_groups = parameters.get("securityGroups", {})
        parameter_tags = {}
        if parameters.get("tags"):
            parameter_tags = {x.get("Key"): x.get("Value") for x in parameters.get("tags")}
        launch_templates = {}

        for launch_template_name, launch_template in core_servers.items():
            error = ClientState.data_check(launch_template, ClientState._default_template)
            if error:
                table = Table(title="Launch template Validation Errors", show_lines=True)
                table.add_column("Launch template name", justify="center", vertical="middle")
                table.add_column("Errors")
                table.add_row(launch_template_name, json.dumps(error, indent=2))
                console = Console()
                console.print(table)
                if force or not continue_with_error():
                    logger.info(f"Skipping launch template {launch_template_name} due to validation errors")
                    continue
            if launch_template.get("ignore"):
                continue
            extra_data = {}
            network_interfaces = {
                "Description": launch_template_name,
                'AssociatePublicIpAddress': launch_template.get("publicIPAction"),
                'Groups': [parameter_security_groups.get(security_group) for security_group in
                           launch_template.get("securityGroups") if security_group in parameter_security_groups],
                'SubnetId': parameter_subnets.get(launch_template.get("subnet"), {}).get("id"),
                "PrivateIpAddress": launch_template.get("ip")
            }
            if "deleteOnTerminationInterface" in launch_template:
                network_interfaces["DeleteOnTermination"] = launch_template["deleteOnTerminationInterface"]
            if "networkInterfaceId" in launch_template:
                network_interfaces["NetworkInterfaceId"] = launch_template["networkInterfaceId"]

            block_device_mappings = []
            if "disks" in launch_template:
                for disk_name, disk_data in launch_template.get("disks").items():
                    if disk_name is None or disk_data is None:
                        continue
                    device = {
                        "DeleteOnTermination": True,
                        "Encrypted": True
                    }
                    if disk_data.get("type"):
                        device["VolumeType"] = disk_data.get("type")
                    if disk_data.get("size"):
                        device["VolumeSize"] = disk_data.get("size")
                    if disk_data.get("iops"):
                        device["Iops"] = disk_data.get("iops")
                    if disk_data.get("throughput"):
                        device["Throughput"] = disk_data.get("throughput")
                    if disk_data.get("kms_key_id"):
                        device["KmsKeyId"] = disk_data.get("kms_key_id")
                    if disk_data.get("snapshot_id"):
                        device["SnapshotId"] = disk_data.get("snapshot_id")

                    block_device_mappings.append({
                        "DeviceName": disk_name,
                        "Ebs": device
                    })
            local_tags = {}
            if launch_template.get("tags"):
                local_tags = {x.get("Key"): x.get("Value") for x in launch_template.get("tags")}
            tags = {**parameter_tags, ** local_tags}
            tags_specifications = [{
                "ResourceType": value,
                "Tags": [{
                    "Key": key,
                    "Value": tags.get(key)
                } for key in tags]
            } for value in ['instance', 'volume', 'network-interface']]

            if "imageId" in launch_template:
                extra_data["ImageId"] = launch_template["imageId"]
            if "keyName" in launch_template:
                extra_data["KeyName"] = launch_template["keyName"]
            if "enableMonitoring" in launch_template:
                extra_data["Monitoring"] = {"Enabled": launch_template["enableMonitoring"]}
            if "disableApiTermination" in launch_template:
                extra_data["DisableApiTermination"] = launch_template["disableApiTermination"]
            if "disableApiStop" in launch_template:
                extra_data["DisableApiStop"] = launch_template["disableApiStop"]
            if "instanceInitiatedShutdownBehavior" in launch_template:
                extra_data["InstanceInitiatedShutdownBehavior"] = launch_template["instanceInitiatedShutdownBehavior"]
            if "userdata" in launch_template:
                extra_data["UserData"] = launch_template.get("userdata")
            if "creditSpecification" in launch_template:
                extra_data["CreditSpecification"] = {"CpuCredits": launch_template["creditSpecification"]}
            launch_templates[launch_template_name] = {
                "InstanceType": launch_template.get("instanceType"),
                "IamInstanceProfile": {"Name": launch_template.get("iamInstanceRole")},
                "NetworkInterfaces": [network_interfaces],
                "BlockDeviceMappings": block_device_mappings,
                "TagSpecifications": tags_specifications,
                **extra_data
            }

        return ClientState(parameter_subnets=parameter_subnets,
                           parameter_security_groups=parameter_security_groups,
                           parameter_tags=parameter_tags,
                           launch_templates=launch_templates)

    @property
    def default_template(self):
        return self.export_default_template(self._default_template)

    def export_default_template(self, default_template: dict) -> dict:
        template = {}

        for key, value in default_template.items():
            if "element" in value:
                template[key] = self.export_default_template(value.get("element"))
            else:
                help_value = value.get("help")
                choices = value.get("choices")
                type_value = value.get("type").__name__
                element = f"{help_value}-Type:{type_value}"
                if choices:
                    element += f"-Possible values:{choices}"
                template[key] = element
        return template

    @staticmethod
    def import_from_extended_source_servers(client, network_interface: NetworkInterface, source_servers: dict):
        launch_templates = {}
        additional_data = {}
        for source_server in source_servers:
            launch_template = source_server.get("launchTemplate", {}).get("LaunchTemplateData")
            name = source_server.get("tags").get("Name")
            instance_type = source_server.get('sourceProperties', {}).get('recommendedInstanceType')
            ip_list = source_server.get('sourceProperties', {}).get('networkInterfaces')
            launch_template_id = drs.get_launch_template_id(client, source_server.get('sourceServerID'))

            if ip_list is None:
                logger.warning(f"No IP found for {name}")
                ip_list = []
            ip = None
            for ip_description in ip_list:
                if ip_description.get("isPrimary"):
                    ip = ip_description.get("ips")[0]
                    break
            launch_templates[name] = launch_template
            additional_data[name] = {
                "launchTemplateId": launch_template_id,
                "ip": ip,
                "instance_type": instance_type
            }
        return ClientState(network_interface=network_interface,
                           launch_templates=launch_templates,
                           additional_data=additional_data)

    def export_as_dict(self, show_default: bool = False):
        state_as_dict = {
            "parameters": {"subnets": self.parameter_subnets, "securityGroups": self.parameter_security_groups},
            "launchTemplates": {}
        }
        if show_default:
            state_as_dict["launchTemplates"]["default_template"] = self.default_template
        for launch_template in self.launch_templates:
            launch_template_as_dict = {}
            launch_template_data = self.launch_templates.get(launch_template)
            network_interface = launch_template_data.get("NetworkInterfaces")[0]
            if network_interface.get("PrivateIpAddress"):
                launch_template_as_dict["ip"] = network_interface.get("PrivateIpAddress")
            else:
                launch_template_as_dict["ip"] = PrivateIPType.CREATE_NEW.value
            subnet_id = network_interface.get("SubnetId", "TO BE DETERMINED")
            if self.mapping_subnets and subnet_id in self.mapping_subnets:
                subnet_id = self.mapping_subnets.get(subnet_id)
            launch_template_as_dict["subnet"] = subnet_id
            security_groups = network_interface.get("Groups", ["TO BE DETERMINED"])
            if security_groups != ["TO BE DETERMINED"] and self.mapping_security_groups:
                security_groups = [self.mapping_security_groups.get(security_group, security_group) for security_group
                                   in security_groups]
            launch_template_as_dict["securityGroups"] = security_groups
            instance_type = self.additional_data.get(launch_template).get("instance_type")
            if instance_type:
                launch_template_as_dict["instanceType"] = instance_type
            if "AssociatePublicIpAddress" in network_interface:
                launch_template_as_dict["publicIPAction"] = network_interface.get("AssociatePublicIpAddress")
            if "DeleteOnTermination" in network_interface:
                launch_template_as_dict["deleteOnTerminationInterface"] = network_interface.get("DeleteOnTermination")
            if "NetworkInterfaceId" in network_interface:
                launch_template_as_dict["networkInterfaceId"] = network_interface.get("NetworkInterfaceId")
            if "IamInstanceProfile" in launch_template_data:
                launch_template_as_dict["iamInstanceRole"] = launch_template_data["IamInstanceProfile"]["Name"]
            launch_template_as_dict["disks"] = {}
            for device in launch_template_data.get("BlockDeviceMappings"):
                device_value = {}
                if device.get("Ebs").get("VolumeType"):
                    device_value["type"] = device.get("Ebs").get("VolumeType")
                if device.get("Ebs").get("VolumeSize"):
                    device_value["size"] = device.get("Ebs").get("VolumeSize")
                if device.get("Ebs").get("Iops"):
                    device_value["iops"] = device.get("Ebs").get("Iops")
                if device.get("Ebs").get("SnapshotId"):
                    device_value["snapshot_id"] = device.get("Ebs").get("SnapshotId")
                if device.get("Ebs").get("KmsKeyId"):
                    device_value["kms_key_id"] = device.get("Ebs").get("KmsKeyId")
                if device.get("Ebs").get("Throughput"):
                    device_value["throughput"] = device.get("Ebs").get("Throughput")
                if device_value != {}:
                    launch_template_as_dict["disks"][device.get("DeviceName")] = device_value

            if "ImageId" in launch_template_data:
                launch_template_as_dict["imageId"] = launch_template_data.get("ImageId")
            if "KeyName" in launch_template_data:
                launch_template_as_dict["keyName"] = launch_template_data.get("KeyName")
            if "Monitoring" in launch_template_data:
                launch_template_as_dict["enableMonitoring"] = launch_template_data.get("Monitoring").get("Enabled")
            if "DisableApiTermination" in launch_template_data:
                launch_template_as_dict["disableApiTermination"] = launch_template_data.get("DisableApiTermination")
            if "DisableApiStop" in launch_template_data:
                launch_template_as_dict["disableApiStop"] = launch_template_data.get("DisableApiStop")
            if "InstanceInitiatedShutdownBehavior" in launch_template_data:
                launch_template_as_dict["instanceInitiatedShutdownBehavior"] = launch_template_data.get(
                    "InstanceInitiatedShutdownBehavior")
            if "UserData" in launch_template_data:
                launch_template_as_dict["userdata"] = launch_template_data.get("UserData")
            if "CreditSpecification" in launch_template_data:
                launch_template_as_dict["creditSpecification"] = launch_template_data.get("CreditSpecification").get(
                    "CpuCredits")
            if "TagSpecifications" in launch_template_data and len(launch_template_data["TagSpecifications"]) > 0:
                launch_template_as_dict["tags"] = launch_template_data["TagSpecifications"][0]["Tags"]
            state_as_dict["launchTemplates"][launch_template] = launch_template_as_dict
        return state_as_dict

    @staticmethod
    def get_complete_launch_template(current_template, new_template, additional_data):
        template = current_template

        if new_template.get("InstanceType"):
            template["InstanceType"] = new_template.get("InstanceType")

        if template["InstanceType"] is None:
            template["InstanceType"] = additional_data.get("instance_type")

        if new_template.get("NetworkInterfaces"):
            network_interfaces = []

            for network_interface in current_template.get("NetworkInterfaces"):
                network_interface = ClientState.get_concatenate_dict(network_interface,
                                                                     new_template.get("NetworkInterfaces")[0])
                # If PrivateIpAddress is CREATE_NEW, we don't send IP and AWS create an IP
                # If PrivateIpAddress is COPY_SOURCE we put source server's IP
                if "PrivateIpAddress" in network_interface:
                    if network_interface.get("PrivateIpAddress") == PrivateIPType.CREATE_NEW.value or \
                            network_interface.get("PrivateIpAddress") is None:
                        network_interface.pop("PrivateIpAddress")
                    elif network_interface.get("PrivateIpAddress") == PrivateIPType.COPY_SOURCE.value:
                        network_interface["PrivateIpAddress"] = additional_data.get("ip")
                network_interfaces.append(network_interface)
            template["NetworkInterfaces"] = network_interfaces

        device_mapping = []
        current_device = {x.get("DeviceName"): x.get("Ebs") for x in
                          current_template.get("BlockDeviceMappings")}
        new_device = {x.get("DeviceName"): x.get("Ebs") for x in new_template.get("BlockDeviceMappings")}

        # for each disk if no value is set in YAML we put default value
        default_disk_data = ClientState.default_parameter.get("disks")
        for device_name in current_device:
            if device_name in new_device:
                new_device_data = ClientState.get_concatenate_dict(default_disk_data, new_device[device_name])
            else:
                new_device_data = default_disk_data

            device_data = ClientState.get_concatenate_dict(current_device[device_name], new_device_data)

            device_mapping.append({
                "DeviceName": device_name,
                "Ebs": device_data
            })
        for device_name in new_device:
            if device_name not in current_device:
                new_device_data = ClientState.get_concatenate_dict(default_disk_data, new_device[device_name])
                device_mapping.append({
                    "DeviceName": device_name,
                    "Ebs": new_device_data
                })

        template["BlockDeviceMappings"] = device_mapping
        override_data = ["IamInstanceProfile", "TagSpecifications", "ImageId", "InstanceType", "KeyName", "Monitoring",
                         "DisableApiTermination", "InstanceInitiatedShutdownBehavior", "UserData",
                         "CreditSpecification", "DisableApiStop"]
        for data_name in override_data:
            if data_name in new_template:
                template[data_name] = new_template[data_name]

        if "UserData" in template:
            template["UserData"] = base64.b64encode(template["UserData"])

        return template

    @staticmethod
    def get_concatenate_dict(current_template, new_template):
        # This method concatenate two dictionaries and if key is in both dictionaries, take the value from new_template
        template = {}
        for key, value in current_template.items():
            if key in new_template:
                if isinstance(value, dict):
                    template[key] = ClientState.get_concatenate_dict(value, new_template[key])
                else:
                    template[key] = new_template[key]
            else:
                template[key] = value
        for key, value in new_template.items():
            if key not in current_template:
                template[key] = value

        return template

    @staticmethod
    def data_check(core_server, default_value):
        wrong_format = {}
        if 'deviceName' in default_value:
            default_value = default_value.get('deviceName').get("element")
        for key, value in core_server.items():
            if key not in default_value:
                logger.info(f"Key {key} not in possible parameter")
                continue
            if "element" in default_value.get(key):
                if value is None:
                    wrong_format[key] = "Missing value"
                    continue
                for child in value:
                    if isinstance(value, dict):
                        value_child = value.get(child)
                    else:
                        value_child = child
                    error = ClientState.data_check(value_child, default_value.get(key).get("element"))
                    if error:
                        if key not in wrong_format:
                            wrong_format[key] = {}
                        wrong_format[key] = error
            else:
                if not isinstance(value, default_value.get(key).get("type")):
                    wrong_format[key] = "Wrong type"
                elif default_value.get(key).get("type") == list:
                    if "elements_type" in default_value.get(key):
                        for child in value:
                            if not isinstance(child, default_value.get(key).get("elements_type")):
                                wrong_format[key] = "Wrong type"
                if "choices" in default_value.get(key):
                    if value not in default_value.get(key).get("choices"):
                        if "IP" in default_value.get(key).get("choices"):
                            if not ClientState.is_ipv4(value):
                                wrong_format[key] = "Wrong value, not a valid IPV4 value"
                        else:
                            wrong_format[key] = "Wrong value"

        return wrong_format

    @staticmethod
    def is_ipv4(string):
        try:
            ipaddress.IPv4Network(string)
            return True
        except ValueError:
            return False
