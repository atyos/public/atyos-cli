from components.drs.ressources.ClientState import ClientState
from components.drs.ressources.NetworkInterface import NetworkInterface
from components.drs.ressources.ec2 import get_ec2_launch_template, get_vpc, get_subnets, get_security_groups


def get_source_server(drs_client):
    # List the source servers for the DR environment
    response = drs_client.describe_source_servers()

    if "items" in response:
        return response["items"]
    return None


def complete_source_servers(drs_client, ec2_client, source_servers):
    for server in source_servers:
        launch_template = get_launch_template(drs_client, ec2_client, server)
        if launch_template:
            server["launchTemplate"] = launch_template
    return source_servers


def get_launch_template(drs_client, ec2_client, source_server):
    launch_template_id = get_launch_template_id(drs_client, source_server.get('sourceServerID'))
    if launch_template_id is not None:
        template = get_ec2_launch_template(ec2_client, launch_template_id)
        return template

    return None


def get_launch_template_id(drs_client, source_server_id):
    response = drs_client.get_launch_configuration(
        sourceServerID=source_server_id
    )
    return response.get('ec2LaunchTemplateID')


def get_current_state(drs_client, ec2_client):
    # get source servers
    source_servers = get_source_server(drs_client)

    # Add launch_templates to source templates
    source_servers = complete_source_servers(drs_client, ec2_client, source_servers)

    vpc_s = get_vpc(ec2_client)

    vpc_ids = [x.get('VpcId') for x in vpc_s]
    subnets = get_subnets(ec2_client, vpc_ids)
    security_groups = get_security_groups(ec2_client, vpc_ids)

    network_interface = NetworkInterface(vpc_s, subnets, security_groups)
    client_state = ClientState.import_from_extended_source_servers(drs_client,
                                                                   source_servers=source_servers,
                                                                   network_interface=network_interface)
    return client_state
