from components.drs.ressources.ec2 import get_name


class NetworkInterface:

    def __init__(self, vpcs: list, subnets: list, security_groups: list):
        mapping = {}
        self._data = {}
        for vpc in vpcs:
            vpc_id = vpc.get('VpcId')
            for tag in vpc.get("Tags"):
                if tag.get("Key") == "Name":
                    mapping[vpc_id] = tag.get("Value")
                    break

        mapping_subnets = {}
        mapping_security_groups = {}
        parameter_subnets = {}
        parameter_security_groups = {}
        for subnet in subnets:
            name = get_name(subnet.get('Tags'))
            vpc_id = subnet.get('VpcId')
            vpc_name = mapping.get(vpc_id)
            name = f"{vpc_name}-{name}"
            subnet_id = subnet.get('SubnetId')
            cidr = subnet.get('CidrBlock')
            parameter_subnets[name] = {
                "id": subnet_id,
                "cidr": cidr
            }
            mapping_subnets[subnet_id] = name

        for security_group in security_groups:
            name = security_group.get('GroupName')
            vpc_id = security_group.get('VpcId')
            vpc_name = mapping.get(vpc_id)
            name = f"{vpc_name}-{name}"
            sg_id = security_group.get('GroupId')
            parameter_security_groups[name] = sg_id
            mapping_security_groups[sg_id] = name

        self.mapping_subnet = mapping_subnets
        self.mapping_security_groups = mapping_security_groups
        self.parameter_subnets = parameter_subnets
        self.parameter_security_groups = parameter_security_groups

    @staticmethod
    def extract_subnet_from_template_data(template_data):
        network_interface = template_data.get('LaunchTemplateData', {}).get('NetworkInterfaces', [])

        if len(network_interface) > 0:
            return network_interface[0].get('SubnetId')

        return None

    @staticmethod
    def extract_security_groups_from_template_data(template_data):
        network_interface = template_data.get('LaunchTemplateData', {}).get('NetworkInterfaces', [])

        if len(network_interface) > 0:
            return network_interface[0].get('Groups')

        return None
