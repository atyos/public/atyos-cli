#!/usr/bin/env python
import json
import logging
import os
import re
import sys
from typing import List, Optional, Dict

import yaml

from components_base import AwsComponent, ComponentConfigMode

logger = logging.getLogger("sam-templates.deploy")
logger.setLevel(logging.getLevelName(os.getenv("LOG_LEVEL", "INFO")))

TEMPLATES_REGEX = re.compile(r"^(:?(?P<key>[0-9]+)-)?(?P<name>.+?)(?:\.(?P<type>yaml|yml|json))?$")
BUILD_DIR = "/tmp/aws-builds"


class DeployComponent(AwsComponent):
    LAYERS_ARN: Dict

    def __init__(self, app: "App", configuration):
        super(DeployComponent, self).__init__(app, configuration)

        logger.info("Initializing SAM Templates Deploy")

        self.cli_args.extend([
            {"name": "--templates-path", "flag": "-t", "help": "Path to templates root", "required": True},
            {"name": "--s3-bucket", "flag": "-b", "help": "S3 bucket to upload artifacts", "required": True},
            {"name": "--project", "flag": "-p", "help": "Project name", "required": False},
            {"name": "--environment", "flag": "-e", "help": "Environment name", "required": False},
            {"name": "--stack-names-format", "flag": "-f", "help": "Template for the stack names", "required": True,
             "default": "$PROJECT-$ENVIRONMENT-$TEMPLATE_NAME"},
            {"name": "--tags", "nargs": "+", "help": "Global tags to be set on resources"},
            {"name": "--parameters", "nargs": "+",
             "help": "Parameters to be set on stacks, can be a a string or Yaml/Json file"},
            {"name": "--role-arn", "help": "Role to pass to Cloudformation for deployment", "required": False},
            {"name": "--dry-run", "action": "store_true", "default": False,
             "help": "Dry run, executes command without deploying templates"},
            {"name": "--use-container", "action": "store_true", "default": False,
             "help": "Use this flag to build your function inside an AWS Lambda-like Docker container"},
            {"name": "--config", "is_config_file": True, "help": "Read configuration from file"},
            {"name": "--sam-args", "required": False,
             "help": "Some custom arguments to pass to sam_cli during invocation"},
            {"name": "--output-layer", "required": False,
             "help": "Output path for dotenv file with latest arn of layers"},
            {"name": "--layers", "action": "store_true", "default": False,
             "help": "define if this is a layer deployment"},
        ])

        if app.component_config_mode is ComponentConfigMode.CLI:
            self._parse_cli_args()
        else:
            logger.error("Module can only be used with CLI config mode")
            sys.exit(1)

    def _format_variable(self, input_str: str) -> str:
        if "$ENVIRONMENT" in input_str:
            if self.configuration.get("environment"):
                input_str = input_str.replace("$ENVIRONMENT", self.configuration.get("environment"))
            else:
                raise ValueError("Formatting requires --environment parameter")
        if "$PROJECT" in input_str:
            if self.configuration.get("project"):
                input_str = input_str.replace("$PROJECT", self.configuration.get("project"))
            else:
                raise ValueError("Formatting requires --project parameter")

        return input_str

    def _format_tags(self) -> Optional[List[str]]:
        tags = self.configuration.get("tags")
        if tags is None:
            return None

        result = []
        try:
            for tag in tags:
                result.append(self._format_variable(tag))
        except ValueError as e:
            raise RuntimeError(f"Tags: {e}")

        return result

    def _format_parameters(self) -> Optional[str]:
        stacks_parameters = self.configuration.get("parameters")
        if stacks_parameters is None:
            return None

        stacks_parameters = " ".join(stacks_parameters)

        # Check if parameters are given as a ParamList (Param1=Value Param2=" Value2=" ...)
        regex = r"^(?: ?[^ =]+=(?:'.+'|\".+\"|[^= ]+))*$"
        if re.match(regex, stacks_parameters):
            return stacks_parameters

        # Check if parameters are given as a Yaml or Json string
        try:
            parameters = yaml.safe_load(stacks_parameters)
        except yaml.YAMLError as e:
            logger.debug(f"Parameters: --parameters does not contain JSON or Yaml. {e}")

        # Check if parameters are given as a file path (Yaml or Json)
        if os.path.isfile(stacks_parameters):
            logger.debug("Parameters: --parameters contains a file path")
            if stacks_parameters.endswith(".yaml") or stacks_parameters.endswith(".yml"):
                try:
                    with open(stacks_parameters, "r") as fh:
                        parameters = yaml.safe_load(fh)
                except yaml.YAMLError as e:
                    logger.error(f"Parameters: Invalid Yaml file \"{stacks_parameters}\". {e}")
                    sys.exit(1)
                except OSError as e:
                    logger.error(f"Parameters: Could not open/read file \"{stacks_parameters}\". {e}")
                    sys.exit(1)
            elif stacks_parameters.endswith(".json"):
                try:
                    with open(stacks_parameters, "r") as fh:
                        parameters = json.load(fh)
                except json.JSONDecodeError as e:
                    logger.error(f"Parameters: Invalid Json file \"{stacks_parameters}\". {e}")
                except OSError as e:
                    logger.error(f"Parameters: Could not open/read file \"{stacks_parameters}\". {e}")
                    sys.exit(1)

        result = ""
        for param, value in parameters.items():
            result += f"{param}=\"{self._format_variable(value)}\" "

        return result[:-1]

    @staticmethod
    def get_list_layers_name(template_path):
        filename = os.path.basename(template_path)
        template_info = re.match(TEMPLATES_REGEX, filename)
        template_name = template_info.group("name")
        with open(f"{BUILD_DIR}/{template_name}/template.yaml", "r") as fh:
            template = yaml.safe_load(fh)
        resources = template.get('Resources', {})
        layers_name = []
        for resource_name, resource_data in resources.items():
            if resource_data.get('Type') == 'AWS::Serverless::LayerVersion':
                layers_name.append(resource_data.get('LayerName', resource_name))
        return layers_name

    def deploy(self, template_path):
        filename = os.path.basename(template_path)
        template_info = re.match(TEMPLATES_REGEX, filename)
        template_name = template_info.group("name")

        stack_name = self.configuration.get("stack_names_format")
        if "$ENVIRONMENT" in stack_name:
            if self.configuration.get("environment"):
                stack_name = stack_name.replace("$ENVIRONMENT", self.configuration.get("environment"))
            else:
                raise RuntimeError("Stack names formatting requires --environment parameter")

        if "$PROJECT" in stack_name:
            if self.configuration.get("project"):
                stack_name = stack_name.replace("$PROJECT", self.configuration.get("project"))
            else:
                raise RuntimeError("Stack names formatting requires --project parameter")

        stack_name = stack_name.replace("$TEMPLATE_NAME", template_info.group("name"))

        sam_build_command = f"sam build --template-file {template_path}" \
                            f" --build-dir {BUILD_DIR}/{template_name}"

        if self.configuration.get("use_container"):
            sam_build_command += f" --use-container"

        sam_deploy_command = f"sam deploy --template-file {BUILD_DIR}/{template_name}/template.yaml" \
                             f" --stack-name {stack_name}" \
                             f" --s3-bucket {self.configuration.get('s3_bucket')}" \
                             f" --capabilities CAPABILITY_IAM CAPABILITY_NAMED_IAM CAPABILITY_AUTO_EXPAND" \
                             f" --no-fail-on-empty-changeset"

        if self.configuration.get("profile") and self.configuration.get("assume_role") is None:
            sam_deploy_command += f" --profile {self.configuration.get('profile')}"

        if (role_arn := self.configuration.get("role_arn")) is not None:
            sam_deploy_command += f" --role-arn {role_arn}"

        if self.configuration.get("tags"):
            sam_deploy_command += f" --tags {' '.join(self._format_tags())}"

        if self.configuration.get("parameters"):
            sam_deploy_command += f" --parameter-overrides {self._format_parameters()}"

        if self.configuration.get("region"):
            sam_deploy_command += f" --region {self.configuration.get('region')}"

        if self.configuration.get("sam_args"):
            sam_deploy_command += f" {self.configuration.get('sam_args')}"

        logger.info(f"Deploying {template_path} on {stack_name}")

        if self.configuration.get("dry_run"):
            logger.info(sam_build_command)
            logger.info(sam_deploy_command)
        else:
            return_code = os.system(sam_build_command)
            if return_code != 0:
                logger.error(f"AWS SAM build exited with non-zero exit code [{return_code}]")
                logger.error(f"Command: {sam_build_command}")
                sys.exit(1)
            return_code = os.system(sam_deploy_command)
            if return_code != 0:
                logger.error(f"AWS SAM deploy exited with non-zero exit code [{return_code}]")
                logger.error(f"Command: {sam_deploy_command}")
                sys.exit(1)

    @classmethod
    def resolve_templates_dir(cls, path) -> List[str]:
        logger.debug(f"Finding templates in {path}")

        try:
            files = os.listdir(path)
        except FileNotFoundError:
            logger.error(f"Cannot find templates directory at {path}")
            sys.exit(1)

        templates = []
        for file in files:
            matches = re.match(TEMPLATES_REGEX, file)
            if matches is not None and matches.group("key") is not None:
                templates.append({"key": int(matches.group("key")), "file": f"{path}/{file}"})
        templates.sort(key=lambda t: t.get("key"))
        templates = [template["file"] for template in templates]

        result = []
        for template in templates:
            if os.path.isdir(template):
                result.extend(cls.resolve_templates_dir(template))
            else:
                result.append(template)

        return result

    def list_layers(self):
        list_layers = []
        session = self._boto_session()
        lambda_client = session.client('lambda')
        layers_info = lambda_client.list_layers()
        list_layers.extend(layers_info.get('Layers'))
        next_marker = layers_info.get('NextMarker')
        while next_marker:
            layers_info = lambda_client.list_layers(Marker=next_marker)
            list_layers.extend(layers_info.get('Layers'))
            next_marker = layers_info.get('NextMarker')

        return list_layers

    def run(self):
        self._aws_init()
        template_path = self.configuration.get("templates_path")

        if os.path.isdir(template_path):
            templates = self.resolve_templates_dir(os.path.abspath(template_path))
        elif os.path.isfile(template_path) and template_path.endswith(("yml", "yaml")):
            templates = [template_path]
        else:
            logger.error("--templates-path must be either a template or a templates directory")
            sys.exit(1)

        for template in templates:
            self.deploy(template)
        if self.configuration.get("layers"):
            layers_info = self.list_layers()
            if not layers_info:
                logger.error("Cannot find any layer")
                return
            list_layers = []
            for template in templates:
                list_layers.extend(DeployComponent.get_list_layers_name(template))

            layers_arn = {}
            for layer in layers_info:
                if layer.get('LayerName') in list_layers:
                    layers_arn[layer.get('LayerName')] = layer.get('LatestMatchingVersion').get(
                        'LayerVersionArn')
            if output_layer := self.configuration.get("output_layer"):
                if layers_arn == {}:
                    logger.error("No layers were created")
                    sys.exit(1)
                with open(output_layer, 'w') as file:
                    for key, value in layers_arn.items():
                        file.write(f'{key}={value}\n')
            else:
                if layers_arn == {}:
                    logger.error("No layers were created")
                    sys.exit(1)
                for key, value in layers_arn.items():
                    logger.info(f'{key}={value}')
