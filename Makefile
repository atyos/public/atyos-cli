.DEFAULT_GOAL := help

#
# Variables
#
PYTHON = venv/bin/python
LIB_DIR = venv/lib

#
# Commands
#

.PHONY: help
help: ## Show the help
help:
	@printf "\033[33mUsage:\033[0m\n  make [target] [arg=\"val\"...]\n\n\033[33mTargets:\033[0m\n"
	@grep -E '^[-a-zA-Z0-9_\.\/]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "  \033[32m%-15s\033[0m %s\n", $$1, $$2}'

binary: ## Build the binary
binary: $(LIB_DIR) $(PYINSTALLER)
	pyinstaller atyos.spec

.PHONY: requirements
requirements: ## Install requirements
requirements: $(LIB_DIR)

#
# Rules
#

$(PYTHON):
	@python -m venv venv
	@touch $@

$(LIB_DIR): $(PYTHON) requirements.txt
	$(PYTHON) -m pip install -r requirements.txt
	@touch $@
