FROM amazon/aws-sam-cli-build-image-python3.9:latest as builder

ARG TERRAFORM_VERSION=1.7.1

# https://stackoverflow.com/questions/51287437/docker-image-build-getting-check-sum-error-rpmdb-checksum-is-invalid-dcdpt
RUN touch /var/lib/rpm/* && yum update -y && yum install curl unzip -y

# Install Terraform
RUN cd /tmp \
    && mkdir terraform-distributions \
    && cd terraform-distributions \
    && curl -LO "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip" \
    && curl -LO "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_SHA256SUMS" \
    && cat terraform_${TERRAFORM_VERSION}_SHA256SUMS | grep terraform_${TERRAFORM_VERSION}_linux_amd64.zip | sha256sum --check \
    && unzip "terraform_${TERRAFORM_VERSION}_linux_amd64.zip" -d /usr/local/bin/ \
    && chmod ugo+x /usr/local/bin/terraform

ADD . ./

# Build atyos cli binary
RUN pip install pyinstaller && make binary

FROM amazon/aws-sam-cli-build-image-python3.9:latest

# Upgrade and install some tools
# https://stackoverflow.com/questions/51287437/docker-image-build-getting-check-sum-error-rpmdb-checksum-is-invalid-dcdpt
RUN touch /var/lib/rpm/* && yum update -y

# Add Atyos CLI binary
COPY --from=builder /var/task/dist/atyos /usr/local/bin/atyos

# Install Terraform
COPY --from=builder /usr/local/bin/terraform /usr/local/bin/terraform

# Add do_cli in binaries PATH folder
RUN ln -s /usr/local/bin/atyos /usr/local/bin/atyos_cli

WORKDIR /project
#ENTRYPOINT ["/bin/atyos"]
